import {applyMiddleware, compose, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';

import devMiddlewares from './devMiddlewares';
import rootSaga from '../sagas/index';
import rootReducer from '../reducers/index';

import {composeWithDevTools} from "redux-devtools-extension";

import thunk from 'redux-thunk';
import multi from 'redux-multi';
import promise from 'redux-promise';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware, promise, multi, thunk].concat(devMiddlewares);

const enhancer = process.env.NODE_ENV === 'development' ? composeWithDevTools : compose;

const store = createStore(rootReducer, enhancer(applyMiddleware(...middlewares)));

sagaMiddleware.run(rootSaga);

export default store;

