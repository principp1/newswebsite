import axios from "axios";
import moment from "moment-timezone";
import URLParse from 'urlparse';
import store from './stores/configureStore';

// import {dispatch} from 'redux';

import {ActionTypes as types} from './constants';

export const SESSION_KEY = 'sessionInfo';

export const client = axios.create();

client.defaults.baseURL = process.env.REACT_APP_API_URL;

export const getAuthHeaders = () => {
    const session = JSON.parse(window.localStorage.getItem(SESSION_KEY));
    if (!session) {
        return {}
    }
    return {'Authorization': `${session.token_type} ${session.access_token}`};
};


export const isExpired = () => {
    const session = JSON.parse(window.localStorage.getItem(SESSION_KEY));
    if (
        !session ||
        !session.expires ||
        moment.tz(session.expires, 'UTC').local().isBefore(moment())
    ) {
        return true
    }
    return false;
};

export const AUTHENTICATION_URLS = [
    "/api/v1/mfa/check/",
    "/api/v1/mfa/confirm/",
    "/api/v1/authorize/",
    "/api/v1/revoke_token/"
];

export const getRefreshToken = () => {
    const session = JSON.parse(window.localStorage.getItem(SESSION_KEY));
    return session ? session.refresh_token : null;

};


const oauth = axios.create();

const refreshTokenCheck = (config) => new Promise((resolve, reject) => {
    const parsed = URLParse(config.url);
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    };
    if (AUTHENTICATION_URLS.includes(parsed.path) || !isExpired()) {
        return resolve(null);
    } else {
        const params = new URLSearchParams();

        params.append('grant_type', 'refresh_token');
        params.append('refresh_token', getRefreshToken());
        params.append('client_id', process.env.REACT_APP_CLIENT_ID);
        params.append('client_secret', process.env.REACT_APP_CLIENT_SECRET);

        return oauth
            .post(
                `${process.env.REACT_APP_API_URL}/authorize/`,
                params,
                {headers}
            )
            .then(r => {
                const data = {...r.data};
                data.expires = moment().add(data.expires_in, 'seconds').valueOf();
                window.localStorage.setItem(SESSION_KEY, JSON.stringify(data));
                resolve(r.data);
            })
            .catch(r => {
                if (r && r.response && r.response.status === 401) {
                    return store.dispatch({
                        type: types.AUTHENTICATION_LOGOUT,
                        data: {}
                    });
                } else {
                    return reject(null);
                }

            });
    }
});


export const requestInterceptor = (config) => {
    return refreshTokenCheck(config)
        .then(token => {
            if (token !== null) {
                config.headers['Authorization'] = `Bearer ${token.access_token}`;
            }
            return Promise.resolve(config);
        });

};

export const responseInterceptor = (error) => {
    return Promise.reject(error);
};


export const createItem = (id, name, list) => {
    if (list.length > 0){
        return {
            "id": id,
            "parent": {"text": name, "path": `/${id}`},
            "children": list.map(e => ({"path": `/wealth/${e.slug}`, "text": e.title}))
        }
    }
};


export const push = payload => ({
    type: '@@location/PUSH',
    payload
});

