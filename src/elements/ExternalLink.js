import React from 'react';
import { string, node, func } from 'prop-types';

function ExternalLink({ to, children, innerRef, ...props }) {
  const theProps = { ...props, href: to }
  if (innerRef) {
    theProps.ref = innerRef;
  }
  return (
    <a
      target="_blank"
      rel="noopener noreferrer"
      { ...theProps }
    >{ children }</a>
  );
}

ExternalLink.propTypes = {
  to: string,
  children: node,
  innerRef: func,
};

export default ExternalLink;
