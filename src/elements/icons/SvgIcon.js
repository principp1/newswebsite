import React from 'react';
import SVG from 'react-inlinesvg';

function SvgIcon({src, ...props}) {
  return (
    <SVG
      src={ src }
      { ...props }
    />
  );
}

export default SvgIcon;

