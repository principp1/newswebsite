import React from 'react';
import SvgIcon from './SvgIcon';

function ArchiveIcon({src, ...props}) {
  return (
    <SvgIcon
      src="/img/svg/archive.svg"
      { ...props }
    />
  );
}

export default ArchiveIcon;

