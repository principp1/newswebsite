import React from 'react';
import SvgIcon from './SvgIcon';

function SentIcon({src, ...props}) {
  return (
    <SvgIcon
      src="/img/svg/noun_sent_1930846.svg"
      { ...props }
    />
  );
}

export default SentIcon;

