import React from 'react';
import SvgIcon from './SvgIcon';

function InboxIcon({src, ...props}) {
  return (
    <SvgIcon
      src="/img/svg/noun_Inbox_In_1071659.svg"
      { ...props }
    />
  );
}

export default InboxIcon;
