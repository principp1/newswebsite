import React from 'react';
import SvgIcon from './SvgIcon';

function TrashIcon({src, ...props}) {
  return (
    <SvgIcon
      src="/img/svg/noun_bin_2012827.svg"
      { ...props }
    />
  );
}

export default TrashIcon;

