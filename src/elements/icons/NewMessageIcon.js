import React from 'react';
import SvgIcon from './SvgIcon';

function NewMessageIcon({src, ...props}) {
  return (
    <SvgIcon
      src="/img/svg/noun_new_message_729124.svg"
      { ...props }
    />
  );
}

export default NewMessageIcon;
