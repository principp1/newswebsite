import React from 'react';

function ChatIcon(props) {
  return (
    <img
      src="/img/svg/noun_chat_2008302.svg"
      alt="chat"
      className="chat-img"
      { ...props }
    />
  );
}

export default ChatIcon;
