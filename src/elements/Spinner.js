import React from 'react';
import Loader from 'react-loader-spinner';

import './Spinner.scss';

// TODO: Specify this as a "FAST component"? I forget
// what it was
function Spinner() {
  return (
    <div className="Spinner">
      <Loader
        type="Oval"
        color="#f3d300"
        height={60} width={60}
      />
    </div>
  );
}

export default Spinner;
