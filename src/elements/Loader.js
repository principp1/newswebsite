import React from 'react';
import { string, node, bool, func, oneOfType } from 'prop-types';

import Spinner from './Spinner';
import './Loader.scss';

function Loader({ children, loaded, className }) {
  return loaded
    ? (typeof children === "function" ? children() : children )
    : (<div className={`Loader ${className}`}><Spinner /></div>);
}

Loader.propTypes = {
  children: oneOfType([node, func]).isRequired,
  loaded: bool.isRequired,
  className: string
};

export default Loader;
