import React from 'react';
import { string, node } from 'prop-types';

function Row({ className, children }) {
  return (
    <div className={ `row ${className}` }>{ children }</div>
  );
}

Row.propTypes = {
  className: string,
  children: node.isRequired
};

export default Row;
