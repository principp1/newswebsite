import React, {Component} from 'react';
import LoginWrap from "../../components/login/LoginWrap";
import Card from "../../components/login/Card";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {confirmReset} from './actions';
import '../../components/login/LoginPage.scss';
import {ConfirmPasswordRecoveryForm} from "../../components/login/forms";
import {Helmet} from "react-helmet";

class ForgotPassword extends Component {
    render() {
        const {confirmReset} = this.props;
        const {match: {params: {uuid}, path}} = this.props;
        const create = path.includes('create');
        return (
            <LoginWrap>
                <Helmet>
                    <title>{create ? 'Password create' : 'Change password'}</title>
                </Helmet>
                <Card title={create ? 'Password create' : 'Change password'}>
                    <ConfirmPasswordRecoveryForm
                        onSubmit={data => confirmReset(uuid, data, create)}
                    />
                </Card>
            </LoginWrap>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch =>
    bindActionCreators({
        confirmReset
    }, dispatch);

ForgotPassword = connect(
    mapStateToProps,
    mapDispatchToProps
)(ForgotPassword);

export default ForgotPassword;
