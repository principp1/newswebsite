import React, {Component} from 'react';
import LoginWrap from "../../components/login/LoginWrap";
import Card from "../../components/login/Card";
import {RequestPasswordRecoveryForm} from "../../components/login/forms/index";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {requestReset} from './actions';
import '../../components/login/LoginPage.scss';
import {Helmet} from "react-helmet";

class ForgotPassword extends Component {
    render() {
        const {requestReset} = this.props;
        return (
            <LoginWrap>
                <Helmet>
                    <title>Password Reset</title>
                </Helmet>
                <Card title="Password Reset">
                    <RequestPasswordRecoveryForm
                        onSubmit={data => requestReset(data)}
                    />
                </Card>
            </LoginWrap>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch =>
    bindActionCreators({
        requestReset
    }, dispatch);

ForgotPassword = connect(
    mapStateToProps,
    mapDispatchToProps
)(ForgotPassword);

export default ForgotPassword;
