// import {client as axios, getAuthHeaders} from "../../utils";
import axios from 'axios';
import { SubmissionError } from "redux-form";

const client = axios.create();

export const requestReset = data => dispatch =>
    client.post(`${process.env.REACT_APP_API_URL}/reset/password/`, data)
        .then(r => {
            alert('Password reset generated, check your email!');
            window.location.href = '/login';
        })
        .catch(r => {
            throw new SubmissionError({
                ...(r.response ? r.response.data : {}),
                _error: r.response.status === 404 ? 'User not found' : 'Please provide a valid email address'
            })
        });

export const confirmReset = (uuid, data, create) => dispatch =>
    client.post(
        `${process.env.REACT_APP_API_URL}/reset/password/${uuid}/`, data)
        .then(r => {
            alert(create ? 'Password successfully created!' : 'Password successfully changed!');
            window.location.href = '/login';
        })
        .catch(r => {
            console.log("error: ", r.response.data);
            throw new SubmissionError({
                ...(r.response ? r.response.data : {}),
                _error: r.response.status === 404 ?
                    'User not found. Maybe your reset token is invalid!' :
                    r.response.data.password_confirm ? r.response.data.password_confirm[0] : r.response.data.password[0]
            })
        });
