
const INITIAL_STATE = {
    benefitPage: null
};


export default (state = INITIAL_STATE, action) => {
    const { type, payload } = action;
    switch(type){
        case '@@benefits/FETCH':
            return {
                ...state,
                benefitPage: payload,
            };
        default:
            return state;
    }
}