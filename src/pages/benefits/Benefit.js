import React, {Component} from 'react';

import MainLayout from '../../components/layout/MainLayout';
import Banner from '../../components/benefits/Banner';
import BenefitContent from '../../components/benefits/BenefitContent';
import Provider from '../../components/benefits/Provider';
import Faq from '../../components/benefits/Faq';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {fetch} from './actions';
import {Helmet} from "react-helmet";
class BenefitPage extends Component {
    constructor(props) {
        super(props);
        const {fetch, slug} = this.props;
        fetch(slug)
    }

    render() {

        const {data, type} = this.props;

        if (!data) {
            return (
                <MainLayout
                    bodyClass={`${type}-page`}
                    footerTitle={data ? data.title : null}
                    loading={!data}
                />
            );
        }
        const {
            title,
            contentSection,
            contactHrWidget,
            mortgageReminderWidget,
            provider,
            faq,
        } = data;
        return (

            <MainLayout
                bodyClass={`${type}-page`}
                footerTitle={data ? data.title : null}

            >
                <Helmet>
                    <title>{data.title}</title>
                </Helmet>
                <Banner
                    {...data}
                    status={data.status} // TODO: Integrate employee data
                />
                <BenefitContent
                    title={title}
                    contents={contentSection}
                    contactHrWidget={contactHrWidget}
                    mortgageReminderWidget={mortgageReminderWidget}
                />
                <Provider provider={provider}/>
                <Faq items={faq}/>
            </MainLayout>
        );
    }
}

BenefitPage = connect(
    state => ({data: state.benefits.benefitPage}),
    dispatch => bindActionCreators({
        fetch
    }, dispatch)
)(BenefitPage);

export default BenefitPage;