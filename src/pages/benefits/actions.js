import {client as axios, getAuthHeaders} from "../../utils";

export const fetch = slug => dispatch => {
    dispatch({type: '@@benefits/FETCH', payload: null});
    return axios.get(`/cms/${slug}/`, {headers: getAuthHeaders()})
        .then(r => dispatch({type: '@@benefits/FETCH', payload: r.data}))
};