import React, { Component } from 'react';
import { func } from 'prop-types';

class LogoutModal extends Component {
  static propTypes = {
    onConfirm: func,
  };

  constructor(props) {
    super(props);
    this.handleConfirm = this.handleConfirm.bind(this);
  }

  handleConfirm(e) {
    this.props.onConfirm();
  }

  render() {
    return (
      <div className="modal fade" id="logoutModal" tabIndex={-1} role="dialog" aria-labelledby="logoutModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <p>Are you sure you want to log out? </p>
              <div className="row">
                <div className="col-6">
                  <button
                    onClick={this.handleConfirm}
                    className="btn btn-grey btn-block btn-lg rounded-0"
                    data-dismiss="modal"
                  >Yes</button>
                </div>
                <div className="col-6">
                  <button
                    className="btn btn-primary btn-block btn-lg rounded-0"
                    data-dismiss="modal"
                  >No</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LogoutModal;
