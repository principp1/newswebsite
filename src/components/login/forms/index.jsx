import React from 'react';
import QRCode from 'qrcode.react';

import { Link } from 'react-router-dom';
import { Field, formValueSelector, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Input, Password } from "../../forms/fields";

const FORM_NAME = 'google-mfa-login';

const selector = formValueSelector(FORM_NAME);

class GoogleMFAForm extends React.Component {
    render() {
        const { handleSubmit, secret, barcode_uri, submitting } = this.props;

        return <form onSubmit={handleSubmit} className="login-form">
            {barcode_uri && <div>
                <div className="form-group">
                    <label>Scan this QRCode</label>
                    <div className='qrcode-container'>
                        <QRCode value={barcode_uri.replace('%40', '@')} renderAs={'svg'} />
                    </div>
                </div>
                <div className="form-group">
                    <label>or type this secret:</label>
                    <div>
                        <strong className={'text-white secret'}>{secret}</strong>
                    </div>
                </div>

            </div>}

            <Field
                name='otp'
                component={Input}
                placeholder={'000000'}
                disabled={submitting}
            />
            <button
                disabled={submitting}
                type="submit"
                className="btn btn-primary rounded-0 login-btn"
            >Continue
            </button>
        </form>
    }
}

GoogleMFAForm = connect(state => ({
    secret: selector(state, 'secret'),
    barcode_uri: selector(state, 'barcode_uri')
}))(GoogleMFAForm);

GoogleMFAForm = reduxForm({
    form: FORM_NAME,
    enableReinitialize: true,
})(GoogleMFAForm);


class RequestPasswordRecoveryForm extends React.Component {
    render() {
        const { handleSubmit, error, submitting } = this.props;
        return <form onSubmit={handleSubmit} className="login-form">
            {error && <div className="message-alert">
                <img src="/img/tick-3.png" alt="tick" />
                {error}
            </div>}
            <Field
                disabled={submitting}
                placeholder='email@host.com'
                name='email'
                component={Input}
            />
            <button
                type="submit"
                className="btn btn-primary rounded-0 login-btn forgot-btn"
                disabled={submitting}
            >
                Request Password Reset
            </button>
            <Link
                to="/login"
                className="forget-password">
                ← Back to Login
            </Link>
        </form>
    }
}

RequestPasswordRecoveryForm = reduxForm({
    form: 'request-passwordd-recovery',
})(RequestPasswordRecoveryForm);

class ConfirmPasswordRecoveryForm extends React.Component {
    render() {
        const { handleSubmit, submitting, error } = this.props;
        return <form onSubmit={handleSubmit} className="login-form">
            {error && <div className="message-alert">
                <div>
                    <img src="/img/tick-4.png" alt="tick" width='35'/>
                </div>
                {error}
            </div>}
            <Field
                name='password'
                label='Password'
                disabled={submitting}
                placeholder="********"
                component={Password}
            />
            <Field
                name='password_confirm'
                label='Password confirm'
                disabled={submitting}
                placeholder="********"
                component={Password}
            />
            <button
                type="submit"
                className="btn btn-primary rounded-0 login-btn forgot-btn"
                disabled={submitting}
            >
                Create password
            </button>
            <Link
                to="/login"
                className="forget-password">
                ← Back to Login
            </Link>
        </form>
    }
}

ConfirmPasswordRecoveryForm = reduxForm({
    form: 'confirm-passwordd-recovery',
})(ConfirmPasswordRecoveryForm);

export {
    GoogleMFAForm,
    RequestPasswordRecoveryForm,
    ConfirmPasswordRecoveryForm
}