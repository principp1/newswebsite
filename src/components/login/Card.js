import React from 'react';
import { node, string } from 'prop-types';

function Card({ children, title }) {
  return (
    <div className="login-modal">
      <h2 className="login-header">{ title }</h2>
      { children }
    </div>
  );
}

Card.propTypes = {
  children: node,
  title: string,
};

export default Card;
