import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import LoginWrap from './LoginWrap';
import Card from './Card';
import './style.css';
import {GoogleMFAForm} from "./forms";


class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            rememberMe: '',
        };

        this.handleLogin = this.handleLogin.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }

    handleLogin(e) {
        e.preventDefault();
        const {username, password, rememberMe} = this.state;
        this.props.onLogin({
            username,
            password,
            otp: '',
            rememberMe: rememberMe === "" ? false : true,
            redirectTo: '/',
        });
    }

    changeHandler(field) {
        return (e) => {
            const stateObj = {};
            stateObj[field] = e.target.value;
            this.setState(stateObj);
        }
    }

    renderLogoutNotification() {
        return this.props.showLogoutSuccessMessage ? (
            <div className="logout-alert">
                <img src="/img/tick-2.png" alt="tick"/>
                You have successfully logged out!
            </div>
        ) : null;
    }


    renderLoginFailMessage() {
        const {showLoginFailureMessage} = this.props;
        return showLoginFailureMessage ? (
            <div className="logout-alert">
                {
                    typeof showLoginFailureMessage === "string"
                        ? showLoginFailureMessage
                        : "Login failed. Check your credentials."
                }

            </div>
        ) : null;
    }

    renderLoginForm() {
        const disabled = this.props.loginInProgress;

        return (
            <form onSubmit={this.handleLogin} action="" className="login-form">
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Your E-mail</label>
                    <input
                        onChange={this.changeHandler('username')}
                        type="email"
                        className="form-control"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                        placeholder="mailadress@gmail.com"
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input
                        onChange={this.changeHandler('password')}
                        type="password"
                        className="form-control"
                        id="exampleInputPassword1"
                        placeholder="Password"
                    />
                </div>
                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input
                            onChange={this.changeHandler('rememberMe')}
                            type="checkbox"
                            className="custom-control-input"
                            id="customCheck1"
                        />
                        <label className="custom-control-label" htmlFor="customCheck1">Keep me signed in</label>
                    </div>
                </div>
                <button
                    type="submit"
                    className="btn btn-primary rounded-0 login-btn"
                    disabled={disabled}
                >Login
                </button>
                {/* <Link to="/forgot-password" className="forget-password">Forgot Password or Username?</Link> */}
                <Link to="/forgot-password" className="forget-password"> Forgotten username or password?</Link>
            </form>
        );
    }

    render() {
        const {mfaToken, barcodeUri, secret, onMFAConfirm} = this.props;
        return (
            <LoginWrap>
                {this.renderLogoutNotification()}
                {this.renderLoginFailMessage()}
                <Card title="Login">
                    {mfaToken ?
                        <GoogleMFAForm
                            onSubmit={({otp}) => onMFAConfirm({
                                mfa_token: mfaToken,
                                otp
                            })}
                            initialValues={{
                                barcode_uri: barcodeUri,
                                secret
                            }}
                        /> : this.renderLoginForm()}
                </Card>
            </LoginWrap>
        );
    }
}

export default LoginPage;

