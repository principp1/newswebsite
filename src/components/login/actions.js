import axios from 'axios';

const client = axios.create();

export const fetch = () => dispatch => {
    const url = `${process.env.REACT_APP_API_URL}/cms/login-page/`;
    return client.get(url)
        .then(r => dispatch({type: '@@login-page/SET', payload: r.data}));
};
