import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import LoginWrap from './LoginWrap';
import Card from './Card';

import './LoginPage.scss';

class ForgotPasswordPage extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
  };

  renderForm() {
    return (
      <form action="" className="login-form">
        <div className="form-group">
          <label htmlFor="exampleInputEmail1">Your E-mail</label>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            placeholder="mailadress@gmail.com"
          />
        </div>
        <button
          type="submit"
          className="btn btn-primary rounded-0 login-btn forgot-btn"
          disabled={ false }
        >Request Password Reset</button>
        <Link to="/login" className="forget-password">← Back to Login</Link>
      </form>
    );
  }

  render() {
    return (
      <LoginWrap>
        <Card title="Password Reset">{ this.renderForm() }</Card>
      </LoginWrap>
    );
  }
}

export default ForgotPasswordPage;
