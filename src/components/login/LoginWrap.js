import React from 'react';

import SiteContainer from '../SiteContainer';
import Main from '../Main';
import { fetch } from './actions';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import rawHtml from "../../helpers/rawHtml";

class LoginWrap extends React.Component {
    constructor(props) {
        super(props);
        const { fetch } = this.props;
        fetch();
        // alert('hey')

    }
    render() {
        const { children, title, logo, content } = this.props;
        return (
            <SiteContainer bodyClass="login-page">
                <Main>
                    <div className="login-wrapper-2">
                        <div className="wrap h-100">
                            <div
                                className="row h-100 align-items-center justify-content-center align-items-center no-gutters">
                                <div className="col-12 col-xl-6">
                                    <div
                                        className="row align-items-center align-items-stretch no-gutters flex-xl-nowrap -left-wrap">
                                        <div className="col-12 col-xl-auto">
                                            <div className="caption-wrap d-flex align-items-center bg-trans">
                                                <div className="hgroup" >
                                                    {logo && <img src={logo} className="responImage" alt="cycle" />}
                                                    {title && <h1>{title}</h1>}
                                                    {content && <p className="set-text" dangerouslySetInnerHTML={rawHtml(content)} />}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 col-xl-6">
                                            {children}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Main>
            </SiteContainer>
        );
    }
}

LoginWrap = connect(
    state => ({ ...state.layout.loginPage }),
    dispatch => bindActionCreators({ fetch }, dispatch)
)(LoginWrap);

export default LoginWrap;
