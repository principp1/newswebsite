import {client as axios, getAuthHeaders} from "../../utils";

// {type: '@@cms/entries/SET', payload: null}

export const fetch = () => dispatch => axios.get('cms/', {headers: getAuthHeaders()})
    .then(r => dispatch({type: '@@cms/entries/SET', payload: r.data}));