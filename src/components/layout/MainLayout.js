import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Loader from '../../elements/Loader';
import SiteContainer from '../SiteContainer';
import Main from '../Main';
import Header from '../Header';
import Footer from '../footer/Footer';
import {fetch} from './actions';
import '../MainLayout.scss'

import AlertModal from '../alertmodal';

class MainLayout extends Component {
    constructor(props) {
        super(props);
        const {fetch} = this.props;
        fetch();
    }

    render() {
        const {loading, bodyClass, footerTitle} = this.props;
        return (
            <SiteContainer className="MainLayout" bodyClass={bodyClass}>
                <Header/>
                <Main>
                    <Loader loaded={!loading}>
                        <div className="revealer">{this.props.children}</div>
                    </Loader>
                </Main>
                <Footer title={footerTitle}/>
                <AlertModal/>
            </SiteContainer>
        );
    }
}

MainLayout = connect(null, dispatch => bindActionCreators({
    fetch
}, dispatch))(MainLayout);

export default MainLayout;
