const INITIAL_STATE = {
    entries: null,
    privacyPolicy: null,
    loginPage: null,
    modal: null,
    flex: null,
    homecards: null,
};

export default (state = INITIAL_STATE, action) => {
    const {type, payload} = action;
    switch(type){
        case '@@flex/SET':
            return {
                ...state,
                flex: payload,
            };
        case '@@location/PUSH':
            window.location.href = payload;
            return {
                ...state,
            };
        case '@@modal/SHOW':
            return {
                ...state,
                modal: payload,
            };
        case '@@modal/HIDE':
            return {
                ...state,
                modal: null,
            };
        case '@@login-page/SET':
            return {
                ...state,
                loginPage: payload
            };
        case '@@cms/privacy-policy/FETCH':
            return {
                ...state,
                privacyPolicy: payload,
            };
        case '@@cms/entries/SET':
            return {
                ...state,
                entries: payload
            };
        case '@@cms/homecards/SET':
            return {
                ...state,
                homecards: payload
            };
        default:
            return state
    }
}