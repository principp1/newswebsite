import React from 'react';

class Input extends React.Component {
    render() {
        const {input, label, type, placeholder, disabled, meta: {touched, error}} = this.props;

        return <div className="form-group is-invalid">
            <label>{label}</label>
            <input
                disabled={disabled}
                {...input}
                type={type || 'text'}
                className="form-control"
                placeholder={placeholder || ''}
            />
            {touched && error && <small className="text-danger">{error}</small>}


        </div>
    }
}

class ImageInput extends React.Component {
    constructor(props) {
        super(props);
        this.input = React.createRef();

        // this.onChange = this.onChange.bind(this);
        // this.state = {preview: null}

    }

    // onChange(e) {
    //     const {input: {value, onChange}} = this.props;
    //     onChange(e.target.files[0]);
    //     const reader = new FileReader();
    //     reader.onloadend = () => {
    //         this.setState({preview: reader.result})
    //     }
    //     if (value && value instanceof File) {
    //         reader.readAsDataURL(value);
    //     }
    // }

    render() {
        const {input, label, disabled} = this.props;
        const {value, onChange} = input;

        return <div className="form-group">
            {value && !(value instanceof File) &&
            <img src={value} alt="" className='img-fluid'/>}
            {/*{value ? value instanceof File ? value.name : value : ''}*/}
            {label && <label>{label}</label>}

            <input
                required={false}
                ref={this.input}
                onChange={e => onChange(e.target.files[0])}
                disabled={disabled}
                type='file'
                className="form-control hidden-file"
            />
            <button
                type="button"
            onClick={e => this.input.current.click()}
            className="btn btn-load rounded-0">
            Choose your ımage
            </button>
        </div>
    }
}


const Password = props => <Input {...props} type='password'/>

class Checkbox extends React.Component {
    render() {
        const {input, label, disabled} = this.props;
        const {name, value} = input;
        const INPUT_ID = `id_${name}`;
        return <div className="form-group form-check">
            <input
                {...input}
                checked={value ? 'checked' : ''}
                disabled={disabled}
                type="checkbox"
                className="form-check-input"
                id={INPUT_ID}/>
            <label
                className="form-check-label"
                htmlFor={INPUT_ID}>
                {label}
            </label>

        </div>
    }
}


export {Input, Checkbox, Password, ImageInput};