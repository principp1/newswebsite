import {client as axios, getAuthHeaders} from "../../utils";

export const getHomeCards = () => dispatch => axios.get(
    `cms/home-cards/`,
    {headers: getAuthHeaders(),}
)
    .then(r => dispatch({
        type: '@@cms/homecards/SET',
        payload: r.data
    }))
    .catch(r => console.log(r.response));

