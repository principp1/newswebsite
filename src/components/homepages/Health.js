import React, {Component} from 'react';

import MainLayout from '../layout/MainLayout';
import HomeWrap from './HomeWrap';
import GridItem from './GridItem';

import './Health.scss';
import {connect} from "react-redux";
import {Helmet} from "react-helmet";

class Health extends Component {
    render() {
        let {entries} = this.props;
        entries = (entries || []).filter(e => e.group === 'health');
        const top = entries.filter((item, index) => index < 2);
        const bottom = entries.filter((item, index) => index >= 2);
        // const hcp = "Health<br /> Cash Plan";
        return (
            <MainLayout
                bodyClass="home-page home-wealth home-health"
                loading={!entries}
            >
                <Helmet>
                    <title>Health</title>
                </Helmet>
                <HomeWrap>
                    <div className='group-flex group-2'>
                        {top.map((e, index) => <GridItem
                            title={e.title}
                            text={e.card_text}
                            key={index}
                            justifyStart
                            to={`/wealth/${e.slug}`}
                        />)}
                    </div>
                    <div className='group-flex group-3'>
                        {bottom.map((e, index) => <GridItem
                            title={e.title}
                            text={e.card_text}
                            key={index}
                            justifyStart
                            to={`/wealth/${e.slug}`}
                        />)}
                    </div>

                    {/*<GridItem*/}
                    {/*title="Private Medical Insurance"*/}
                    {/*text="Private medical insurance (PMI) offers quick access to eligible health treatment, from seeing a specialist for diagnosis to having a comfortable stay whilst in hospital, such as staying in a private room rather than being on a ward."*/}
                    {/*to="/health/your-private-medical-insurance"*/}
                    {/*span={6}*/}
                    {/*mb={30}*/}
                    {/*/>*/}

                    {/*<GridItem*/}
                    {/*title="Dental Insurance"*/}
                    {/*text="Group dental insurance helps pay towards the ever increasing costs of dental treatment through an insurance policy and allows employees to claim back the cost of routine and unexpected dental treatment."*/}
                    {/*to="/health/your-dental-insurance"*/}
                    {/*span={6}*/}
                    {/*mb={30}*/}
                    {/*/>*/}

                    {/*<GridItem*/}
                    {/*title="Health Screening"*/}
                    {/*text="Health Assessments are an assessment of your physical health, providing you with an understanding of your physical ability and helping you to understand your body better and be aware of how today’s lifestyle can affect your body, as well as actions you can take to improve your wellbeing."*/}
                    {/*to="/health/your-health-screening"*/}
                    {/*span={4}*/}
                    {/*mb={30}*/}
                    {/*h={402}*/}
                    {/*/>*/}

                    {/*<GridItem*/}
                    {/*title={hcp}*/}
                    {/*text="Health Cash Plans provide reimbursement for many of you and your families everyday health expenses, such as dental check-ups and treatment, optical tests and prescription eyewear."*/}
                    {/*to="/health/your-health-cash-plan"*/}
                    {/*span={4}*/}
                    {/*mb={30}*/}
                    {/*h={402}*/}
                    {/*/>*/}

                    {/*<GridItem*/}
                    {/*title="Employee Assistance Programme"*/}
                    {/*text="Employee Assistance Programmes (EAPs) provide support services to employees and their immediate family members and help them deal with personal problems and/or work-related problems that may affect adversely impact their work performance, health and mental or emotional wellbeing."*/}
                    {/*to="/health/your-eap"*/}
                    {/*span={4}*/}
                    {/*mb={30}*/}
                    {/*h={402}*/}
                    {/*/>*/}

                </HomeWrap>
            </MainLayout>
        );
    }
}

Health = connect(state => ({entries: state.layout.entries}))(Health);
export default Health;
