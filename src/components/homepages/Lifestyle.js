import React, {Component} from 'react';

import MainLayout from '../layout/MainLayout';
import HomeWrap from './HomeWrap';
import GridItem from './GridItem';

import './Lifestyle.scss';
import {connect} from "react-redux";
import {Helmet} from "react-helmet";

class Lifestyle extends Component {

    render() {
        let {entries} = this.props;
        entries = (entries || []).filter(e => e.group === 'lifestyle');
        return <MainLayout
            bodyClass="home-page home-lifestyle"
        >
            <Helmet>
                <title>Lifestyle</title>
            </Helmet>
            <HomeWrap>
                <div className='group-flex group-2'>
                    {entries.map((e, index) => <GridItem
                        title={e.title}
                        text={e.card_text}
                        key={index}
                        to={`/wealth/${e.slug}`}
                        justifyStart
                    />)}
                </div>

            </HomeWrap>
        </MainLayout>;
    }
}

Lifestyle = connect(state => ({entries: state.layout.entries}))(Lifestyle);
export default Lifestyle;
