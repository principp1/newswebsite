import React, {Component} from 'react';

import MainLayout from '../layout/MainLayout';
import HomeWrap from './HomeWrap';
import GridItem from './GridItem';
import {connect} from 'react-redux';
import {Helmet} from "react-helmet";
//import FlexWindow from './FlexWindow';

class Wealth extends Component {
    render() {
        let {entries} = this.props;
        entries = (entries || []).filter(e => e.group === 'wealth');
        const top = entries.filter((item, index) => index < 3);
        const bottom = entries.filter((item, index) => index >= 3);
        return (
            <MainLayout
                bodyClass="home-page home-wealth"
                loading={!entries}
            >
                <Helmet>
                    <title>Wealth</title>
                </Helmet>
                <HomeWrap>
                    <div className='group-flex group-3'>
                        {top.map((e, index) => <GridItem
                            key={index}
                            title={e.title}
                            text={e.card_text}
                            justifyStart
                            to={`/wealth/${e.slug}`}
                        />)}

                    </div>
                    <div className="group-flex group-2">
                        {bottom.map((e, index) => <GridItem
                            key={index}
                            title={e.title}
                            text={e.card_text}
                            justifyStart
                            to={`/wealth/${e.slug}`}
                        />)}
                    </div>
                </HomeWrap>
            </MainLayout>
        );
    }
}

Wealth = connect(state => ({entries: state.layout.entries}))(Wealth);

export default Wealth;
