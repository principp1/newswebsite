import {client as axios, getAuthHeaders} from "../../../utils";


export const fetch = () => dispatch => axios.get('/widgets/flex/', {headers: getAuthHeaders()})
    .then(r => dispatch({type: '@@flex/SET', payload: r.data}))
    .catch(r => console.log('error ignored!'));