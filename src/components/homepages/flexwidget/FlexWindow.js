import React, { Component } from 'react';
import './flex-widget.scss';
import moment from 'moment-timezone';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetch } from './actions';

const getCountDown = d => {
    // Get todays date and time
    const now = new Date().getTime();

    // Find the distance between now and the count down date
    const distance = d - now;
    if (distance < 0) return null;

    // Time calculations for days, hours, minutes and seconds
    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);
    return { days, hours, minutes, seconds };
};

class FlexWindow extends Component {
    constructor(props) {
        super(props);
        const { fetch } = this.props;
        fetch();
        this.state = {
            open: null,
            close: null,
        };
        setInterval(() => {
            const { open_at, close_at } = this.props;
            const open = open_at ? moment(open_at) : null;
            const close = close_at ? moment(close_at) : null;
            if (open) {
                this.setState({ open: getCountDown(open.toDate()) });
            }
            if (close) {
                this.setState({ close: getCountDown(close.toDate()) });
                // this.props.isOpen({ closeDays: this.state.close.days })
            }
        }, 1000);
    }

    render() {
        const { open, close } = this.state;
        if (!close && !open) return null;
        const { description, title } = this.props;
        return (
            <div className='grid-item-wrapper'>
                <div className="grid-item grid-item-yellow">
                    {title && <h1>{title}</h1>}
                    {close && description && <p>{description}</p>}
                    {open && <React.Fragment>
                        <p>Flex <strong>opens</strong> in:</p>
                        <div className="d-flex">
                            <div>
                                <div className='flex-digit-title'>DAYS</div>
                                <div className="flex-widget-digit">
                                    {('00' + open.days).slice(-3)}
                                </div>
                                <div className="dot">:</div>
                            </div>
                            <div>
                                <div className='flex-digit-title'>HOURS</div>
                                <div className="flex-widget-digit">
                                    {open.hours >= 10 ? open.hours : '0' + open.hours}
                                </div>
                                <div className="dot">:</div>
                            </div>
                            <div>
                                <div className='flex-digit-title'>MINS</div>
                                <div className="flex-widget-digit">
                                    {open.minutes >= 10 ? open.minutes : '0' + open.minutes}
                                </div>
                                <div className="dot">:</div>
                            </div>
                            <div>
                                <div className='flex-digit-title'>SECS</div>
                                <div className="flex-widget-digit">
                                    {open.seconds >= 10 ? open.seconds : '0' + open.seconds}
                                </div>
                            </div>
                        </div>
                    </React.Fragment>}
                    {close && <React.Fragment>
                        <p>Flex <strong>closes</strong> in:</p>
                        <div className="d-flex">
                            <div>
                                <div className='flex-digit-title'>DAYS</div>
                                <div className="flex-widget-digit">
                                    {('00' + close.days).slice(-3)}
                                </div>
                                <div className="dot">:</div>
                            </div>

                            <div>
                                <div className='flex-digit-title'>HOURS</div>
                                <div className="flex-widget-digit">
                                    {close.hours >= 10 ? close.hours : '0' + close.hours}
                                </div>
                                <div className="dot">:</div>
                            </div>
                            <div>
                                <div className='flex-digit-title'>MINS</div>
                                <div className="flex-widget-digit">
                                    {close.minutes >= 10 ? close.minutes : '0' + close.minutes}
                                </div>
                                <div className="dot">:</div>
                            </div>
                            <div>
                                <div className='flex-digit-title'>SECS</div>
                                <div className="flex-widget-digit">
                                    {close.seconds >= 10 ? close.seconds : '0' + close.seconds}
                                </div>
                            </div>
                        </div>
                    </React.Fragment>}

                </div>
            </div>
        );
    }
}

FlexWindow = connect(
    state => ({ ...state.layout.flex }),
    dispatch => bindActionCreators({ fetch }, dispatch)
)(FlexWindow);

export default FlexWindow;
