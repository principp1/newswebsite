import React, { Component } from 'react';
import { number, node, bool, string, func } from 'prop-types';

class GridContainer extends Component {
  static propTypes = {
    children: node,
    className: string,
    onClick: func,
    span: number,
    h: number,
    mt: number,
    hb: number,
    mb: number,
    xlAuto: bool,
    dFlex: bool,
    alignCenter: bool,
  };

  containerClass() {
    const { span, mb, xlAuto, className } = this.props;
    const classes = ["col-12", `col-lg-${span}`, className];
    if (xlAuto) {
      classes.push(" col-xl-auto");
    }
    if (mb) {
      classes.push(` mb-${mb}`);
    }
    return classes.join(" ");
  }

  blockClasses() {
    const { dFlex, alignCenter } = this.props;
    const classes = ["home-block"];
    if (dFlex) classes.push("d-flex");
    if (alignCenter) classes.push("align-items-center");
    ["h", "hb", "mt"].forEach(prop => {
      if (this.props[prop] > 0) {
        classes.push(`${prop}-${this.props[prop]}`);
      }
    });
    return classes.join(" ");
  }

  render() {
    return (
      <div className={ this.containerClass() }>
        <div
          className={ this.blockClasses() }
          onClick={ this.props.onClick }
        >
          <div className="hgroup">
            { this.props.children }
          </div>
        </div>
      </div>
    );
  }
}

export default GridContainer;

