import React, { Component } from 'react';

import MainLayout from '../layout/MainLayout';
import HomeWrap from './HomeWrap';
import GridItem from './GridItem';
import FlexWindow from './flexwidget/FlexWindow';
import { bindActionCreators } from 'redux';
import './HomePage.scss';
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { getHomeCards } from "./actions";

class HomePage extends Component {
    state = {
        isBanner: false
    };

    // isOpen(data) {
    //     if (data.closeDays >= 1) {
    //         this.setState({ isBanner: true });
    //     }
    // }
    constructor(props){
        super(props);
        const { getHomeCards } = this.props;
        getHomeCards();
    }
    render() {
        const { entries } = this.props;
        const { homecard_wealth_title, homecard_wealth_text } = this.props;
        const { homecard_health_title, homecard_health_text } = this.props;
        const { homecard_lifestyle_title, homecard_lifestyle_text } = this.props;
        const { homecard_documents_title, homecard_documents_text } = this.props;
        const hasHealth = (entries || []).filter(e => e.group === 'health').length > 0;
        const hasWealth = (entries || []).filter(e => e.group === 'wealth').length > 0;
        const hasLifeStyle = (entries || []).filter(e => e.group === 'lifestyle').length > 0;
        return (
            <MainLayout
                bodyClass="home-page"
                loading={!entries}
            >
                <Helmet>
                    <title>Your benefits</title>
                </Helmet>
                <HomeWrap>
                    {/* {this.state.isBanner && <h1 style={{ marginLeft: 7, color: '#f3d300', fontFamily: 'Playfair Display', fontWeight: '900' }}>Flex window is currently open</h1>} */}
                    <div className='group-flex'>
                        {hasWealth && <GridItem
                            title={homecard_wealth_title}
                            text={homecard_wealth_text}
                            to="/wealth"
                        />}

                        {hasHealth && <GridItem
                            title={homecard_health_title}
                            text={homecard_health_text}
                            to="/health"
                        />}


                        <FlexWindow
                            span={4}
                            hb={3}
                            h={310}
                            xlAuto
                            dFlex
                            alignCenter
                            // isOpen={data => this.isOpen(data)}
                        />
                    </div>
                    <div className='group-flex'>

                        <GridItem
                            title={homecard_documents_title}
                            text={homecard_documents_text}
                            to="/documents"
                        />

                        {hasLifeStyle && <GridItem
                            title={homecard_lifestyle_title}
                            text={homecard_lifestyle_text}
                            to="/lifestyle"
                        // span={6}
                        // h={220}
                        // mt={55}
                        // dFlex
                        // alignCenter
                        />}

                    </div>
                </HomeWrap>
            </MainLayout>
        );
    }
}

HomePage = connect(
    state => ({
        entries: state.layout.entries,
        ...state.layout.homecards
    }),
    dispatch => bindActionCreators({ getHomeCards }, dispatch)
)(HomePage);
export default HomePage;
