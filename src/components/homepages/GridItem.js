import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import rawHtml from '../../helpers/rawHtml';
import ExternalLink from '../../elements/ExternalLink';

// import GridContainer from './GridContainer';

import './GridItem.scss';

class GridItem extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        if (e.target.closest('a') !== this.link) {
            this.link.click();
        }
    }

    renderLink(path, text) {
        const {title} = this.props;
        const display = <span dangerouslySetInnerHTML={rawHtml(title)}/>;
        return path.match(/^https?:\/\//)
            ? <ExternalLink
                to={path}
                innerRef={a => this.link = a}
            >{display}</ExternalLink>
            : <Link
                to={path}
                innerRef={a => this.link = a}
            >{display}</Link>
    }

    render() {
        const {title, text, to, justifyStart} = this.props;
        return <div className='grid-item-wrapper'>
            <div
                className={`grid-item top-margin ${justifyStart ? 'flex-justify-start' : ''}`}

                onClick={this.handleClick}
            >
                <h1>
                    {this.renderLink(to, title)}
                </h1>
                <p dangerouslySetInnerHTML={rawHtml(text)}/>
            </div>
        </div>

    }
}

export default GridItem;
