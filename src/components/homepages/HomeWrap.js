import React from 'react';
import {node} from 'prop-types';

function HomeWrap({children}) {
    return (
        <div className="home-wrap">
            <div className="wrap">
                {children}
            </div>
        </div>
    );
}

export default HomeWrap;
