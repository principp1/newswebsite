import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { arrayOfLinks, linkData } from '../propTypes';
import NavItem from './NavItem';

import './NavDropDown.scss';

class NavDropDown extends Component {
    static propTypes = {
        parent: linkData,
        children: arrayOfLinks,
        className: PropTypes.string,
    };

    constructor(props) {
        super(props);
        const { parent } = props;
        this.state = { show: false, hovered: false };
        this.navId = `navbarDropdown-${parent.text.toLowerCase()}`;
        this.handleHover = this.handleHover.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);
        this.listenToDropdownShow = this.listenToDropdownShow.bind(this);
    }

    componentDidMount() {
        document.addEventListener('navDropDownShow', this.listenToDropdownShow);
    }

    componentWillUnmount() {
        document.removeEventListener('navDropDownShow', this.listenToDropdownShow);
    }

    listenToDropdownShow(e) {
        if (!this.state.hovered) {
            this.setState({ show: false });
        }
    }

    handleHover(e) {
        this.announceShowing();
        this.setState({ show: true, hovered: true });
    }

    announceShowing() {
        const event = document.createEvent('Event');
        event.initEvent('navDropDownShow', true, true);
        document.dispatchEvent(event);
    }

    handleMouseLeave(e) {
        this.setState({ hovered: false });
        setTimeout(() => {
            this.setState(state => {
                if (!state.hovered) {
                    return { show: false };
                }
            })
        }, 300);
    }

    renderDivider(i) {
        return (
            <li key={`divider-${i}`}>
                <div className="dropdown-divider"></div>
            </li>
        )
    }

    renderLink(link) {
        const { path, text } = link;
        const linkNode = path.match(/^https?:\/\//)
            ? <a href={path} className="d_link_item" target="_blank" rel="noopener noreferrer">{text}</a>
            : <Link to={path} className="d_link_item">{text}</Link>

        return (
            <li key={path}>{linkNode}</li>
        );
    }

    renderMenu() {
        return this.props.children.map((link, i) => (
            link.divider ? this.renderDivider(i) : this.renderLink(link)
        ));
    }

    navClass(match) {
        return "nav-item dropdown" + (match ? " active" : "");
    }

    render() {
        const { parent } = this.props;
        return (
            <NavItem
                path={parent.path}
                className="nav-dropdown"
                onMouseOver={this.handleHover}
                onMouseLeave={this.handleMouseLeave}
            >
                <Link
                    to={parent.path}
                    className="nav-link dropdown-toggle"
                    id={this.navId}
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                >{parent.text}{" "}</Link>
                <div
                    className={`dropdown-menu ${this.state.show ? "show" : ""}`}
                    aria-labelledby={this.navId}
                >
                    <div className="d_menu d-flex align-items-center">
                        <div className="cell d-none d-lg-block">
                            {parent.text === "Health" && <img style={{ borderBottomLeftRadius: 20, height: 219 }} src="/img/Wealth_image.png" alt="menu-img" />}
                            {parent.text === "Wealth" && <img style={{ borderBottomLeftRadius: 20 }} src="/img/Health_image.png" alt="menu-img" />}
                            {parent.text === "Lifestyle" && <img style={{ borderBottomLeftRadius: 20 }} src="/img/menu-img.png" alt="menu-img" />}
                        </div>
                        <ul className="d_links">
                            {this.renderMenu()}
                        </ul>
                    </div>

                </div>

            </NavItem>
        );
    }
}

export default NavDropDown;
