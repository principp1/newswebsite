import React, {Component} from 'react';

import MainLayout from '../layout/MainLayout';
import GridItem from '../homepages/GridItem';

import './DocumentsPage.scss';
import {Helmet} from 'react-helmet';

class DocumentsPage extends Component {

    render() {
        return (
            <MainLayout bodyClass="home-page home-documents">
                <Helmet>
                    <title>Documents</title>
                </Helmet>
                <div className="home-wrap">
                    <div className="wrap">
                        <div className="row">
                            <GridItem
                                title="Employee <br />Handbook"
                                text=""
                                to="/documents"
                                span={6}
                                h={220}
                                dFlex
                            />

                            <GridItem
                                title="Holiday <br />Request <br />Form"
                                text=""
                                to="/documents"
                                span={6}
                                h={220}
                                dFlex
                            />

                        </div>
                    </div>
                </div>
            </MainLayout>
        );
    }
}

export default DocumentsPage;
