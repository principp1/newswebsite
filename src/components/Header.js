import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {logout} from '../actions/authenticationActions';
import {NavLink} from 'react-router-dom';
import NavDropDown from './NavDropDown';
import NavIconLink from './NavIconLink';
import NavItem from './NavItem';
import LogoutModal from './login/LogoutModal';

import pageLinks from '../fixtures/pageLinks';

import './Header.scss';
import {createItem} from "../utils";


class Header extends Component {

    render() {
        const {logout, entries} = this.props;
        const benefitsLinks = [];
        if (entries) {
            const wealth = entries.filter(e => e.group === 'wealth');
            const health = entries.filter(e => e.group === 'health');
            const lifestyle = entries.filter(e => e.group === 'lifestyle');
            if (wealth.length > 0) {
                benefitsLinks.push(createItem('wealth', 'Wealth', wealth));
            }
            if (health.length > 0) {
                benefitsLinks.push(createItem('health', 'Health', health));
            }
            if (lifestyle.length > 0) {
                benefitsLinks.push(createItem('lifestyle', 'Lifestyle', lifestyle));
            }

        }
        return (
            <Fragment>
                <header role="banner" className="site-header">
                    <div className="wrap">
                        <nav className="navbar navbar-expand-lg navbar-dark bg-none">
                            <button
                                className="navbar-toggler"
                                type="button"
                                data-toggle="collapse"
                                data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent"
                                aria-expanded="false"
                                aria-label="Toggle navigation"
                            >
                                <span className="navbar-toggler-icon"></span>
                                <span className="toggler-text">Menu</span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav first-nav">
                                    <NavItem path="/" exact>
                                        <NavLink
                                            className="nav-link"
                                            to="/"
                                        >
                                            Your benefits <span className="sr-only">(current)</span>
                                        </NavLink>
                                    </NavItem>
                                    {benefitsLinks.map(({parent, children}) => (
                                        <NavDropDown
                                            key={parent.path}
                                            parent={parent}
                                            children={children}
                                        />
                                    ))}
                                </ul>
                                <ul className="navbar-nav mr-auto mid-nav">
                                    {pageLinks.map(props => (
                                        <NavIconLink key={props.path} {...props} />
                                    ))}
                                </ul>
                                <button
                                    type="button"
                                    className="btn btn-primary"
                                    data-toggle="modal"
                                    data-target="#logoutModal"
                                >
                                    Logout
                                </button>
                            </div>
                        </nav>
                    </div>
                </header>
                <LogoutModal onConfirm={e => logout()}/>
            </Fragment>
        );
    }
}

Header = connect(
    state => ({
        entries: state.layout.entries
    }),
    dispatch => bindActionCreators({logout}, dispatch)
)(Header);

export default Header;