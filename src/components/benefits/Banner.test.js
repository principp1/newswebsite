import React from 'react';
import { shallow } from 'enzyme';

import Img from 'react-image';

import Banner from './Banner';

describe('Banner', () => {
  let component;
  let commonProps = {
    title: 'Your pension',
    bannerImage: '/path/to/img.jpg',
    bannerLayout: 'left',
    intro: "<p>This is some text that has html</p>",
    status: "Eligible",
  };

  const mountBanner = props =>
    shallow(
      <Banner {...props} />
    );

  beforeEach(() => {
    component = mountBanner(commonProps);
  });

  it('shows banner image', () => {
    expect(component.find(Img)).toHaveProp('src', commonProps.bannerImage);
  });

  it('shows status message from status prop', () => {
    expect(component.find(".highlighted-text")).toHaveText("Eligible");
  });
});

