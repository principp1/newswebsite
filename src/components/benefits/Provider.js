import React, {Component} from 'react';

import rawHtml from '../../helpers/rawHtml';
import YouTubeVideo from '../YouTubeVideo';

import './styles.scss';

class Provider extends Component {
    renderProviderLogo() {
        const {name, logo} = this.props.provider;
        if (logo) {
            return (
                <div className="col-12 col-lg-5">
                    <img
                        src={`${logo}`}
                        className="centered-md mb-5 provider-logo" alt={`${name} logo`}/>
                </div>
            );
        }
    }

    renderContent() {
        const {logo, content} = this.props.provider;
        return (
            <div
                className={`col-12 block-text ${ logo ? 'col-lg-7' : '' }`}
                dangerouslySetInnerHTML={rawHtml(content)}
            />
        );
    }

    renderCustomContent() {
        const {customContent} = this.props.provider;
        return (
            <div
                dangerouslySetInnerHTML={rawHtml(customContent)}/>

        )
    }

    render() {
        if (!this.props.provider) return null;
        const {video} = this.props.provider;

        return (
            <section className="content-block">
                <div className="wrap">
                    <div className="row">
                        {this.renderProviderLogo()}
                        {this.renderContent()}
                    </div>
                    {video ? <YouTubeVideo url={video}/> : null}
                    {this.renderCustomContent()}
                </div>
            </section>
        );
    }
}

export default Provider;
