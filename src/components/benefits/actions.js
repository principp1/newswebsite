import {client as axios, getAuthHeaders} from "../../utils";
import {show} from "../alertmodal/actions";


export const save = data => dispatch => axios.post(
    '/messages/mortgage/',
    data,
    {headers: getAuthHeaders()}
)
    .then(r => dispatch(show(
        r.data.message,
        null,
        r.data.type
    )));
