import React, { Component } from 'react';
import PropTypes from 'prop-types';
import voca from 'voca';

import MainLayout from '../layout/MainLayout';

class BenefitsList extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
  };

  render() {
    const { match } = this.props;
    return (
      // List
      <MainLayout bodyClass="home-page">
        <div>
          <h1>{ voca.capitalize(match.params.type) }</h1>
        </div>
      </MainLayout>
    );
  }
}

export default BenefitsList;
