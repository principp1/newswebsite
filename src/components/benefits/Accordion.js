import React, { Component } from 'react';
import PropTypes from 'prop-types';
import rawHtml from '../../helpers/rawHtml';

import { accordionData } from '../../propTypes';

import './Accordion.scss';

class Accordion extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    id: PropTypes.string,
    sections: accordionData
  };

  asId(text) {
    return text.replace(/[\W]+/g, '-').toLowerCase();
  }

  renderSections() {
    const { sections } = this.props;
    if (!sections) return null;
    return sections.map(({ title, content }) => {
      const id = this.asId(title);
      const collapseId = `${id}-collapse`;
      return (
        <div key={id} className="card">
          <div className="card-header" id={id}>
            <h5 className="mb-0">
              <button
                className="btn btn-link collapsed"
                type="button" data-toggle="collapse"
                data-target={`#${collapseId}`}
                aria-expanded="false"
                aria-controls="collapseOne"
              >{ title }</button>
            </h5>
          </div>
          <div
            id={ collapseId }
            className="collapse"
            aria-labelledby="headingOne"
            data-parent="#accordionExample"
          >
            <div
              className="card-body cms-content"
              dangerouslySetInnerHTML={ rawHtml(content) }
            />
          </div>
        </div>
      );
    });
  }

  render() {
    const { id, className } = this.props;
    return (
      <div className={ `accordion ${className}` } id={ id }>
        { this.renderSections() }
      </div>
    );
  }
}

export default Accordion;
