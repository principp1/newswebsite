import React, {Component} from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import './Banner.scss';
import moment from 'moment';
import { save } from './actions';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class MortgageWidget extends Component {
    constructor(props) {
        super(props);

        this.state = {
            date: new Date(),
            email: '',
            telephone: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(e){
        const { save } = this.props;
        e.stopPropagation();
        e.preventDefault();

        save({
            date: moment(this.state.date).format('YYYY-MM-DD'),
            telephone: this.state.telephone,
            email: this.state.email,
        });
    }
    handleChange(date) {
        this.setState({
            date: date
        });
    }

    render() {
        return (
            <div className="mortgage-widget bg-lite">
                <div className="row no-gutters h-100 align-items-center justify-content-center">
                    <div className="col-12">
                        <div className="row no-gutters justify-content-center">
                            <div className="col-12 col-sm-auto">
                                <img src="/img/icon-tick.png" alt="chat" className="chat-img"/>
                            </div>
                            <div className="col">
                                <h1>Mortgage Reminder</h1>
                                <p>Once you've told us about your mortgage renewal date you can forget about it until we
                                    remind you.</p>
                                <form onSubmit={e => this.handleSubmit(e)}>
                                    <div className="row no-gutters f_row">
                                        <div className="col-12 col-sm-6" style={{marginBottom: 5}}>Mortgage renewal
                                            date
                                        </div>
                                    </div>

                                    <div className="row no-gutters f_row">
                                        <div className="col-12 col-sm-6" style={{padding: 0}}>

                                            <div className="col-12 col-sm-12" style={{padding: 0}}>
                                                {/* <input type="text" className="form-control mw-1"
                                                placeholder="Mortgage Renewal Date" /> */}
                                                <DatePicker
                                                    className="form-control mw-1 "
                                                    selected={this.state.date}
                                                    onChange={e => this.setState({date: e})}
                                                />
                                            </div>

                                            <div className="col-12 col-sm-12" style={{padding: 0}}>
                                                <input
                                                    type="tel"
                                                    maxLength={12}
                                                    className="form-control mw-4"
                                                    placeholder="Preferred phone"
                                                    value={this.state.telephone}
                                                    onChange={e => this.setState({
                                                        telephone: e.target.value
                                                    })}
                                                />
                                            </div>
                                            <div className="col-12 col-sm-12" style={{padding: 0}}>
                                                <input
                                                    type="email"
                                                    className="form-control mw-3"
                                                    placeholder="Preferred email"
                                                    value={this.state.email}
                                                    onChange={e => this.setState({
                                                        email: e.target.value
                                                    })}
                                                />
                                            </div>
                                        </div>

                                    </div>
                                    <div className="row no-gutters justify-content-sm-end s_row">
                                        <div className="col-12 col-sm-6" style={{padding: 0}}>
                                            <div className="col-12 col-sm-12" style={{padding: 0}}>
                                                <button type="submit" className="btn rounded-0 btn-block s_btn">
                                                    Send
                                                </button>
                                            </div>
                                        </div>
                                        <div className="col-12 col-sm-6">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

MortgageWidget = connect(
    state => ({}),
    dispatch => bindActionCreators({ save }, dispatch)
)(MortgageWidget);

export default MortgageWidget;
