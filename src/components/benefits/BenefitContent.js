import React, {Component} from 'react';
import {bool, string} from 'prop-types';
import striptags from 'striptags';

import {accordionData} from '../../propTypes';
import Accordion from './Accordion';
import HrWidget from '../widgets/HrWidget';
import MortgageWidget from './MortgageWidget';

import './BenefitContent.scss';

class BenefitContent extends Component {
    static propTypes = {
        title: string,
        contents: accordionData.isRequired,
        contactHrWidget: bool,
        mortgageReminderWidget: bool,
    };

    renderHrWidget() {
        const {contactHrWidget, title} = this.props;
        return contactHrWidget
            ? <HrWidget subject={striptags(title)}/>
            : null;
    }

    renderMortgageWidget() {
        return this.props.mortgageReminderWidget
            ? <MortgageWidget/>
            : null;
    }

    renderWidget() {
        return this.renderHrWidget() || this.renderMortgageWidget()
    }

    sectionClassName() {
        const {contactHrWidget, mortgageReminderWidget} = this.props;
        let className = "what-can-you-do accordian-block bg-lite";
        if (contactHrWidget) {
            className += " -with-widget";
        } else if (mortgageReminderWidget) {
            className += " -with-widget-mortgage";
        }
        return className;
    }

    render() {
        const { contents } = this.props;
        return contents && contents.length > 0 ?
            <section className={this.sectionClassName()}>
                <div className="wrap">
                    <h1 className="accordian-title">What can you do?</h1>
                    <Accordion sections={contents} id="accordionExample"/>
                </div>
                {this.renderWidget()}
            </section>
        : null;
    }
}

export default BenefitContent;

