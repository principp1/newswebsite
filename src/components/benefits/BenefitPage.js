import React, {Component, Fragment} from 'react';

import MainLayout from '../layout/MainLayout';
import Banner from './Banner';
import BenefitContent from './BenefitContent';
import Provider from './Provider';
import Faq from './Faq';

class BenefitPage extends Component {

    renderMainContent() {
        const {data} = this.props;
        const {
            title,
            contentSection,
            contactHrWidget,
            mortgageReminderWidget,
            provider,
            faq,
        } = data;

        return (
            <Fragment>
                <Banner
                    {...data}
                    status={data.statusMessage} // TODO: Integrate employee data
                />
                <BenefitContent
                    title={title}
                    contents={contentSection}
                    contactHrWidget={contactHrWidget}
                    mortgageReminderWidget={mortgageReminderWidget}
                />
                <Provider provider={provider}/>
                <Faq sections={faq}/>
            </Fragment>
        );
    }

    render() {
        const {data, type, loaded} = this.props;
        return (
            <MainLayout
                bodyClass={`${type}-page`}
                footerTitle={data ? data.title : null}
                loading={!loaded}
            >
                {this.renderMainContent()}
            </MainLayout>
        );
    }
}

export default BenefitPage;

