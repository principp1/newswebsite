import React from 'react';

import Accordion from './Accordion';

const Faq = ({items}) => items && items.length > 0 ? <section className="accordian-block -small bg-lite">
    <div className="wrap">
        <h1 className="accordian-title">Frequently asked questions</h1>
        <Accordion sections={items} id="accordionExample2"/>
    </div>
</section> : null;

export default Faq;
