import React from 'react';
import {oneOf, string} from 'prop-types';
import Img from 'react-image';

import rawHtml from '../../helpers/rawHtml';
import './Banner.scss';


function bannerLayoutClass(layout) {
    return layout === "right"
        ? "row justify-content-end align-items-center h-100"
        : "row align-items-center h-100";
}

class Banner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {width: 0};
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);


    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({width: window.innerWidth});
    }

    render() {
        const {title, bannerImage, bannerLayout, background_mobile, intro, status} = this.props;
        return <section className="BenefitBanner banner-style-1 banner-img">
            <div className="bannerImgWrap">
                {this.state.width < 789 && background_mobile ? <Img
                    src={background_mobile}
                    alt="banner"
                    className="img-fluid banner-img small"
                    loader={<div className="bannerImgLoading"></div>}
                /> : <Img
                    src={bannerImage}
                    alt="banner"
                    className="img-fluid banner-img large"
                    loader={<div className="bannerImgLoading"></div>}
                />}


            </div>
            <div className="banner-caption">
                <div className="wrap h-100">
                    <div className={bannerLayoutClass(bannerLayout)}>
                        <div className="col-lg-6">
                            <div className="highlighted-text">{status}</div>
                            <h1
                                className="banner-title"
                                dangerouslySetInnerHTML={rawHtml(title)}
                            />
                            <div
                                className="banner-content"
                                dangerouslySetInnerHTML={rawHtml(intro)}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    }

}

Banner.propTypes = {
    title: string,
    bannerImage: string,
    bannerLayout: oneOf(["left", "right"]),
};

export default Banner;
