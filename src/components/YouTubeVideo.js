import React from 'react';
import PropTypes from 'prop-types';
import 'url-polyfill';

import './YouTubeVideo.scss';

function getVideoId(urlStr) {
    try{
        const url = new URL(urlStr);
        return url.searchParams.get("v");
    }catch(e){
        return null;
    }

}

function YouTubeVideo({url}) {
    const videoId = getVideoId(url);
    return videoId ?
            <div className="YouTubeVideo">
                <iframe
                    className="img-fluid mt-5"
                    src={`https://www.youtube-nocookie.com/embed/${videoId}`}
                    title="Video"
                    frameBorder="0"
                    allow="autoplay; encrypted-media"
                    allowFullScreen></iframe>
            </div> : null;
}

YouTubeVideo.propTypes = {
    url: PropTypes.string.isRequired,
};

export default YouTubeVideo;
