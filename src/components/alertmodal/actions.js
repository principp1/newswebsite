
export const show = (message, hideAction = null, type = 'default') => ({
    type: '@@modal/SHOW',
    payload: { message, type, hideAction }
});

export const close = hideAction => dispatch => {
    const actions = [
        {type: '@@modal/HIDE'},
    ];
    if (hideAction){
        actions.push(hideAction);
    }
    return dispatch(actions);
};