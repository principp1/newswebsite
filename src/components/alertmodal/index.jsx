import React from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { show, close } from './actions';
class AlertModal extends React.Component {
    render() {
        const { modal, close } = this.props;
        if (!modal){
            return null;
        }
        const { message, hideAction, type } = modal || {};
        console.log(type); // use this to style error, default modal...
        return <React.Fragment>
            <div className="modal show" id='alert-modal' style={{display: 'block'}} role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <p>{message}</p>
                            <div className="row">
                                {/*<div className="col-6">*/}
                                    <button
                                        onClick={e => close(hideAction)}
                                        className="btn btn-grey btn-block btn-lg rounded-0"
                                        data-dismiss="modal"
                                    >
                                        Ok
                                    </button>
                                {/*</div>*/}
                                {/*<div className="col-6">*/}
                                    {/*<button*/}
                                        {/*className="btn btn-primary btn-block btn-lg rounded-0"*/}
                                        {/*data-dismiss="modal"*/}
                                    {/*>*/}
                                        {/*No*/}
                                    {/*</button>*/}
                                {/*</div>*/}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id='alert-modal-backdrop' className="modal-backdrop fade show"/>
        </React.Fragment>
    }
}

AlertModal = connect(
    state => ({
        modal: state.layout.modal
    }),
    dispatch => bindActionCreators({
        show, close
    }, dispatch)
)(AlertModal);
export default AlertModal;