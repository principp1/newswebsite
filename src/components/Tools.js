import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

import MainLayout from './layout/MainLayout';
import './Tools.scss';

function textToClipboard (text) {
  const dummy = document.createElement("textarea");
  document.body.appendChild(dummy);
  dummy.value = text;
  dummy.select();
  document.execCommand("copy");
  document.body.removeChild(dummy);
}

class Tools extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = { text: '', source: '' };
    this.handleEditorChange = this.handleEditorChange.bind(this);
    this.handleCopyJson = this.handleCopyJson.bind(this);
  }

  handleEditorChange(source, a, b, editor) {
    this.setState({ text: source, source })
  }

  handleCopyJson(e) {
    textToClipboard(JSON.stringify(this.state.source));
  }

  render() {
    const style = {
      padding: '100px 0',
      margin: '0 auto',
      width: '900px',
      maxWidth: '80%',
    };

    const preStyle = {
      border: '1px solid #ddd',
      backgroundColor: '#f3f3f3',
      padding: '20px',
      margin: '30px 0',
      whiteSpace: 'pre-wrap',
    }

    return (
      <MainLayout>
        <div
          className="Tools"
          style={ style }
        >
          <h1>Tools</h1>
          <hr />
          <h2>Editor</h2>
          <div className="row">
            <div className="col col-6">
              <ReactQuill
                value={ this.state.text }
                onChange={this.handleEditorChange}
              />
            </div>
            <div className="col col-6">
              <pre style={ preStyle }>
                { this.state.source }
              </pre>
            </div>
          </div>

          <button
            className="btn btn-primary"
            onClick={this.handleCopyJson}>Copy JSON</button>
        </div>
      </MainLayout>
    );
  }
}

export default Tools;
