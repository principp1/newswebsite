import React from 'react';
import PropTypes from 'prop-types';
import { Link, Route } from 'react-router-dom';

function navClass(match) {
  return "nav-item" + (match ? " active" : "");
}

function NavIconLink({ path, text, icon, iconWidth }) {
  return (
    <Route
      path={path}
      children={({ match }) => (
        <li className={ navClass(match) }>
          <Link className={ `nav-link l_${icon}` } to={ path }>
            <span className="l_text">{ text }</span>
            { " " }
            <img src={ `/img/svg/${icon}.svg` } width={iconWidth} alt="icon" />
          </Link>
        </li>
      )}
    />
  );
}

NavIconLink.propTypes = {
  path: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
};

export default NavIconLink;
