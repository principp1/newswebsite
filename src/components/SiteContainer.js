import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SiteContainer extends Component {
  static propTypes = {
    children: PropTypes.node,
  };

  hasBodyClass() {
    return !!this.props.bodyClass;
  }

  bodyClasses() {
    return this.hasBodyClass() ? this.props.bodyClass.split(/\s/) : [];
  }

  componentDidMount() {
    if (this.hasBodyClass()) {
      document.body.classList.add(...this.bodyClasses());
    }
  }

  componentWillUnmount() {
    if (this.hasBodyClass()) {
      this.bodyClasses().forEach(name => {
        document.body.classList.remove(name);
      });
    }
  }

  render() {
    return (
      <div className="site-container">
        { this.props.children }
      </div>
    );
  }
}

export default SiteContainer;
