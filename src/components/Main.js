import React from 'react';
import PropTypes from 'prop-types';

function Main({ children }) {
  return (
    <main role="main" className="site_main">
      { children }
    </main>
  );
}

Main.propTypes = {
  children: PropTypes.node
};

export default Main;
