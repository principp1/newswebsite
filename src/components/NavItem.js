import React, { Component } from 'react';
import { node, string, bool, func } from 'prop-types';
import { Route } from 'react-router-dom';

class NavItem extends Component {
  static propTypes = {
    children: node,
    className: string,
    onMouseOver: func,
    onMouseLeave: func,
    path: string,
    exact: bool,
  };

  navClass(match) {
    const { className } = this.props;
    const classes = ["nav-item"];
    if (className) classes.push(className);
    if (match) classes.push("active");
    return classes.join(" ");
  }

  render() {
    const { path, children, exact, onMouseEnter, onMouseLeave, onMouseOver } = this.props;
    return (
      <Route
        path={ path }
        exact={ exact }
        children={({ match }) => (
          <li
            className={ this.navClass(match) }
            onMouseEnter={ onMouseEnter }
            onMouseOver={ onMouseOver }
            onMouseLeave={ onMouseLeave }
          >
            { children }
          </li>
        )}
      />
    );
  }
}

export default NavItem;
