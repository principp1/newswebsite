
const INITIAL_STATE = {
    hr: {},
    morgage: {},
    flex: {}
};

export default (state = INITIAL_STATE, action) => {
    const { type, payload } = action;
    switch(type){
        case '@@widget/SET':
            return {
                ...state,
                ...payload,
            };
        default:
            return state;

    }

}