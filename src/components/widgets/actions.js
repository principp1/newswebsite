import {client as axios, getAuthHeaders} from "../../utils";

export const fetch = name => dispatch => axios.get(
    `widgets/${name}/`,
    {headers: getAuthHeaders(),}
)
    .then(r => {
        const data = {};
        data[name] = r.data;
        dispatch({
            type: '@@widget/SET',
            payload: data
        });
    })
