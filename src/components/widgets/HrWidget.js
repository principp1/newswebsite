import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import queryString from 'query-string';

import ChatIcon from '../../elements/icons/ChatIcon';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetch } from './actions';
class HrWidget extends Component {
    constructor(props){
        super(props);
        const { fetch } = this.props;
        fetch('hr');
    }
    render() {
        const {subject, to} = this.props;
        const searchParams = queryString.stringify({subject, to});
        return (
            <div className="hr-widget-1 bg-lite">
                <div className="row no-gutters h-100 align-items-center justify-content-center">
                    <div className="col-12">
                        <div className="row no-gutters justify-content-center">
                            <div className="col-auto">
                                <ChatIcon width="56"/>
                            </div>
                            <div className="col">
                                <h1>Need to contact HR <br/> about this benefit?</h1>
                                <div className="sep"/>
                                <Link
                                    to={`/messages/new?${ searchParams }`}
                                    className="btn btn-primary btn-lg rounded-0 text-uppercase contact-btn"
                                >
                                    Contact HR
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

HrWidget = connect(
    state => state.widget.hr,
    dispatch => bindActionCreators({
        fetch
    }, dispatch)
)(HrWidget);

export default HrWidget;
