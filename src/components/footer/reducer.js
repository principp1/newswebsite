const INITIAL_VALUES = {
    logo: null,
    content: null,
    title: null,
};

export default (state = INITIAL_VALUES, action) => {
    const { type, payload } = action;
    switch (type){
        case '@@footer/SET':
            return {
                ...state,
                ...payload,
            };
        default:
            return state;
    }
}