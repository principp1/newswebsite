import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import rawHtml from '../../helpers/rawHtml';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {retrieve} from './actions';

import '../Footer.scss';
import {createItem} from "../../utils";
import CookieConsent from "react-cookie-consent";
class Footer extends Component {
    constructor(props) {
        super(props);
        const {retrieve} = this.props;
        retrieve();
    }


    render() {
        const {title, content, logo, employer_logo, entries, gdrp_content} = this.props;
        const benefitsLinks = [];

        if (entries) {
            const wealth = entries.filter(e => e.group === 'wealth');
            const health = entries.filter(e => e.group === 'health');
            const lifestyle = entries.filter(e => e.group === 'lifestyle');
            if (wealth.length > 0) {
                benefitsLinks.push(createItem('wealth', 'Wealth', wealth));
            }
            if (lifestyle.length > 0) {
                benefitsLinks.push(createItem('lifestyle', 'Lifestyle', lifestyle));
            }
            if (health.length > 0) {
                benefitsLinks.push(createItem('health', 'Health', health));
            }

        }
        return (
            <footer role="contentinfo" className="site-footer bg-grey">
                <div className="wrap">
                    {title && <h1 className="footer-title">{title}</h1>}
                    <div className="footer-icons">
                        <div className="row justify-content-between no-gutters">
                            {employer_logo && <img src={employer_logo} alt=''/>}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-6">
                            <div className="row">
                                {benefitsLinks.map(({parent, children}) => (
                                    <div className="col-sm-4" key={parent.path}>
                                        <div className="widget">
                                            <h4 className="widget-title">
                                                {parent.text}
                                            </h4>
                                            <ul className="widget-list">
                                                {children.map(link => (
                                                    <li key={link.path}>
                                                        <Link to={link.path}>{link.text}</Link>
                                                    </li>
                                                ))}
                                            </ul>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="col-xl-6">
                            <div className="row">
                                <div className="col-sm">

                                </div>
                                <div className="col-sm-auto footer-logo">
                                    {logo && <a href="#logo">
                                        <img
                                            src={logo}
                                            className="d-logo"
                                            alt=""
                                        />
                                    </a>}

                                </div>
                            </div>

                            {content && <div
                                className="footer-text"
                                dangerouslySetInnerHTML={rawHtml(content)}
                            />}

                        </div>
                    </div>
                </div>
                <p className="copyright-text">© Zahara Works Limited 2018</p>
                <CookieConsent
                    location="bottom"
                    buttonText="Accept"
                    cookieName="radnew-gdrp"
                    style={{background: "#000"}}
                    buttonStyle={{color: "#333", fontSize: "13px"}}
                >
                    
                    <div
                        dangerouslySetInnerHTML={ rawHtml(gdrp_content) }
                    />
                    {/*This website or its third party tools use cookies which are necessary for it to function properly*/}
                    {/*and required to achieve the purposes illustrated in the cookie policy. If you want to know more or*/}
                    {/*withdraw your consent to all or some of the cookies, please refer to the cookie policy. By closing*/}
                    {/*this banner, scrolling this page, clicking a link or continuing to browse this site, you agree to*/}
                    {/*the use of cookies. Please see our Privacy Policy if you would like further details.*/}
                </CookieConsent>
            </footer>
        )
    }
}

Footer = connect(
    state => ({
        ...state.footer,
        entries: state.layout.entries
    }),
    dispatch => bindActionCreators({
        retrieve
    }, dispatch)
)(Footer);
export default Footer;