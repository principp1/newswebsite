import {client as axios, getAuthHeaders} from "../../utils";

export const retrieve = () => dispatch => axios.get(
    `cms/footer/`,
    {headers: getAuthHeaders(),}
)
    .then(r => dispatch({
        type: '@@footer/SET',
        payload: r.data
    }))
    .catch(r => console.log(r.response));

