import React from 'react';
import { string, node, number } from 'prop-types';
import { Link } from 'react-router-dom';

function SidebarButton({ to, id, active, label, icon, badgeValue, className, ...props }) {


  return (
    <Link
      to={ to }
      className={`
        list-group-item
        d-flex
        justify-content-between
        align-items-center
        ${active ? 'active' : ''}
        ${className ? className : ''}
      `}
      { ...props }
    >
      <span className="-left">
        <span className="svg-box">
          { icon }
        </span>
        { " " }
        { label }
      </span>
      {
        badgeValue > 0
          ? <span className="badge badge-primary badge-pill">{ badgeValue }</span>
          : null
      }
    </Link>

  );
}

SidebarButton.defaultProps = {
  badgeValue: 0,
};

SidebarButton.propTypes = {
  badgeValue: number,
  to: string.isRequired,
  label: string.isRequired,
  icon: node.isRequired,
};

export default SidebarButton;
