import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

import Sidebar from './Sidebar';
import MessagesPane from './MessagesPane';
import MainLayout from '../layout/MainLayout';
import MessageEdit from './MessageEdit';
import ViewMessage from './ViewMessage';
import {Helmet} from "react-helmet";

class MessagesPage extends Component {
    render() {
        return (
            <MainLayout bodyClass="message-page">
                <Helmet>
                    <title>Messages</title>
                </Helmet>
                <section className="message-block">
                    <div className="wrap">
                        <h1 className="profile-heading">Messages</h1>
                        <div className="row align-items-lg-stretch">
                            <Sidebar/>
                            <Switch>
                                <Route
                                    path="/messages/new"
                                    component={MessageEdit}
                                />
                                <Route
                                    path="/messages/:pk/edit"
                                    component={MessageEdit}
                                />
                                <Route
                                    path="/messages/:id/view"
                                    component={ViewMessage}
                                />
                                <Route
                                    path="/messages/:folder"
                                    component={MessagesPane}
                                />
                            </Switch>
                        </div>
                    </div>
                </section>
            </MainLayout>
        )
    }
}

export default MessagesPage;

