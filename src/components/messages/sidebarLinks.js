import NewMessageIcon from '../../elements/icons/NewMessageIcon';
import InboxIcon from '../../elements/icons/InboxIcon';
import SentIcon from '../../elements/icons/SentIcon';
import TrashIcon from '../../elements/icons/TrashIcon';
import ArchiveIcon from '../../elements/icons/ArchiveIcon';

const inbox = {
  id: "inbox",
  to: "/messages/inbox",
  label: "Inbox",
  icon: InboxIcon,
};

const subLinks = [
  {
    id: "sent",
    to: "/messages/sent",
    label: "Sent",
    icon: SentIcon,
  },
  {
    id: "drafts",
    to: "/messages/drafts",
    label: "Drafts",
    icon: NewMessageIcon,
  },
  {
    id: "archive",
    to: "/messages/archive",
    label: "Archive",
    icon: ArchiveIcon,
  },
  {
    id: "trash",
    to: "/messages/trash",
    label: "Trash",
    icon: TrashIcon,
  }
];

const links = [inbox].concat(subLinks);


export function linkFromPath(path) {
  const result = subLinks.find(link => (
    path.startsWith(link.to)
  ));
  return result || inbox;
}

export default links;
