import React from 'react';

function BackToMenuButton({...props}) {
  return (
    <div className="btn btn-primary btn-block compose-btn d-flex align-items-center text-left rounded-0">
      Back to Menu
    </div>
  );
}

export default BackToMenuButton;
