import React, { Component } from 'react';
import { Link, Route, Switch } from 'react-router-dom';

class MessagesListNav extends Component {

  constructor(props) {
    super(props);
    this.renderLinks = this.renderLinks.bind(this);
  }

  renderLinks({ match, location }) {
    const { unreadOnly } = this.props;
    return (
      <ul className="top-links d-flex align-items-center">
        <li>
          <Link
            className={ unreadOnly ? "active" : "" }
            to={ `${match.url}` }
          >Unread</Link>
        </li>
        <li>
          <Link
            className={ unreadOnly ? "" : "active" }
            to={ `${match.url}?all` }
          >All</Link>
        </li>
      </ul>
    );
  }

  render() {
    return (
      <Switch>
        <Route path="/messages/:folder" render={this.renderLinks} />
      </Switch>
    );
  }
}

export default MessagesListNav;
