import React from 'react';
import { node } from 'prop-types';

function NewMessageWrapper({ children }) {
  return (
    <div className="col">
      <div className="compose-box bg-trans mt-3 mt-sm-0">
        { children }
      </div>
    </div>
  );
}

NewMessageWrapper.propTypes = {
  children: node
};

export default NewMessageWrapper;
