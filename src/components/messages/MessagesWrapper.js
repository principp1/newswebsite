import React, { Component } from 'react';
import { node } from 'prop-types';

class MessagesWrapper extends Component {
  static propTypes = {
    children: node,
  };

  render() {
    return (
      <section className="message-block">
        <div className="wrap">
          <h1 className="profile-heading">Messages</h1>
          <div className="row align-items-lg-stretch">
            { this.props.children }
          </div>
        </div>
      </section>

    );
  }
}

export default MessagesWrapper;
