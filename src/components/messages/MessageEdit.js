import React, {Component} from 'react';

import NewMessageWrapper from './NewMessageWrapper';
import {newMessage, fetchRecipients} from '../../actions/messagesActions';
import {Typeahead} from 'react-bootstrap-typeahead';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Field, formValueSelector, reduxForm} from 'redux-form';
import queryString from 'query-string';
import { change as changeFieldValue } from 'redux-form';
import {deleteMessage, fetchMessage} from './actions';

export const FORM_NAME = 'message-edit';

class MultipleSelectInput extends React.Component{
    render() {
        const {label, input, values } = this.props;
        return <div className="row no-gutters f_row">
            <div className="col-auto">
                <label htmlFor={`id_${input.name}`}>{label}</label>
            </div>
            <div className="col">
                <Typeahead
                    multiple
                    onChange={(selected) => input.onChange(selected.map(i => i.id))}
                    options={values}
                    selected={values.filter(v => (input.value || []).includes(v.id))}
                />
            </div>
        </div>
    }
}

class InputArea extends React.Component {
    render() {
        const {label, input} = this.props;
        return <div className="row no-gutters f_row">
            <div className="col-auto">
                <label htmlFor={input.name}>{label}</label>
            </div>
            <div className="col">
                <textarea
                    {...input}
                    className="form-control rounded-0"
                    id={input.name}
                />
            </div>
        </div>
    }
}

class Input extends React.Component {
    render() {
        const {label, input} = this.props;
        return <div className="row align-items-center no-gutters f_row">
            <div className="col-auto">
                <label htmlFor={input.name}>{label}</label>
            </div>
            <div className="col">
                <input
                    {...input}
                    type="text"
                    className="form-control rounded-0"
                    id={input.name}
                />
            </div>
        </div>
    }
}

const selector = formValueSelector(FORM_NAME);

class MessageForm extends React.Component {
    constructor(props){
        super(props);
        const { fetchRecipients, initialValues, changeFieldValue } = this.props;
        fetchRecipients()
            .then(r => {
                if (initialValues && initialValues.to){
                    changeFieldValue(FORM_NAME, 'recipients', [initialValues.to])
                }
            });

    }
    render() {

        const {handleSubmit, onSubmit, draft, deleteMessage, id, recipients, initialValues} = this.props;

        return <form onSubmit={handleSubmit}>
            <Field
                name='recipients'
                component={MultipleSelectInput}
                initialValue={initialValues.to}
                values={recipients}
                label={'To'}
            />
            <Field
                name='subject'
                component={Input}
                label={'Subject'}
            />
            <Field
                name='message'
                component={InputArea}
                label={'Message'}
            />


            <div className="i_row">
                <div className="row align-items-xl-center">
                    <div className="col-12 col-xl-auto">
                        <div className="row align-items-sm-between">
                            {draft && <div className="col-12 col-sm-auto">
                                <button
                                    onClick={e => deleteMessage(id, '/messages/drafts')}
                                    className="btn btn-white">
                                    Remove draft
                                    <i className="fa fa-trash-o"/>
                                </button>
                            </div>}
                            <div className="col-12 col-sm-auto">
                                <button
                                    type="button"
                                    onClick={handleSubmit(values => onSubmit({
                                        ...values,
                                        draft: true
                                    }))}
                                    className="btn btn-white">
                                    Save as Draft
                                    <i className="fa fa-folder-open-o"/>
                                </button>
                            </div>
                            <div className="col-12 col-sm-auto">
                                <button
                                    type="submit"
                                    onClick={handleSubmit(values => onSubmit({
                                        ...values,
                                        draft: false
                                    }))}
                                    className="btn btn-primary"
                                >
                                    Send <i className="fa fa-paper-plane-o"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    }
}

MessageForm = reduxForm({form: FORM_NAME})(MessageForm);
MessageForm = connect(
    state => ({
        draft: selector(state, 'draft'),
        id: selector(state, 'id'),
        recipients: state.messages.recipients,
    }),
    dispatch => bindActionCreators({
        deleteMessage,
        fetchRecipients,
        changeFieldValue
    }, dispatch)
)(MessageForm);

class MessageEdit extends Component {
    constructor(props) {
        super(props);
        const {match, editMessage} = this.props;


        if (match.params && match.params.pk) {
            editMessage(match.params.pk);
        }
    }

    render() {
        const {newMessage} = this.props;
        const initialValues = queryString.parse(this.props.location.search);
        return (
            <NewMessageWrapper>
                <MessageForm
                    initialValues={initialValues}
                    onSubmit={data => newMessage(data)}
                />
            </NewMessageWrapper>
        )
    }
}

MessageEdit = connect(null, dispatch => bindActionCreators({
    newMessage,
    editMessage: fetchMessage
}, dispatch))(MessageEdit);

export default MessageEdit;
