import React, { Component } from 'react';
import { string, func } from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { newMessage } from '../../actions/messagesActions';
import NewMessageIcon from '../../elements/icons/NewMessageIcon';

class NewMessageButton extends Component {
  static propTypes = {
    to: string,
    source: string,
    dispatch: func,
  };

  constructor(props) {
    super(props);
    this.handleNewMessage = this.handleNewMessage.bind(this);
  }

  handleNewMessage(e) {
    const source = this.props.source;
    this.props.dispatch(newMessage({ source }));
  }

  render() {
    return (
      <Link
        to={ this.props.to }
        className="btn btn-primary btn-block compose-btn d-flex align-items-center text-left rounded-0"
      >
        <span className="svg-box">
          <NewMessageIcon />
        </span>
        New Message
      </Link>
    );
  }
}

export default connect(() => {
  return { };
})(NewMessageButton);
