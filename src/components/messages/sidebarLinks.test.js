import { linkFromPath } from './sidebarLinks';

describe('linkFromPath', () => {
  const data = {
    "/messages/sent": "sent",
    "/messages": "inbox",
    "/messages/drafts/foo": "drafts",
    "/messages/nomatch": "inbox",
  };

  for (let path in data) {
    test(`passes correct link for ${path}`, () => {
      expect(linkFromPath(path).id).toEqual(data[path]);
    });
  }
});
