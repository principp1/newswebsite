import React, { Component } from 'react';
import { connect } from 'react-redux';
import { node } from 'prop-types';

import './Search.scss';

class Search extends Component {
  static propTypes = {
    children: node,
  };

  constructor(props) {
    super(props);
    this.state = { query: "" };
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  // TODO: Search implementation
  handleSearch(e) {
    e.preventDefault();
  }

  handleChange(e) {
    this.setState({ query: e.target.value })
  }

  render() {
    return (
      <div className="Search search-block">
        <form onSubmit={ this.handleSearch } className="d-flex align-items-center">
          <button className="searchButton" type="submit"><i className="fa fa-search" /></button>
          <input onChange={ this.handleChange } type="text" className="form-control" placeholder="Search" />
        </form>
      </div>
    );
  }
}

export default connect(state => {
  return {};
})(Search);
