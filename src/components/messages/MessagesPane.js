import React, {Component} from 'react';
import MesssagesListWrapper from './MesssagesListWrapper';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {setFolder} from '../../actions/messagesActions';
import {getMessages} from './actions';
import Loader from "../../elements/Loader";
import MessageListItem from "./MessageListItem";
import {Link} from 'react-router-dom';

import {refreshToken} from "../../actions/authenticationActions";

class MessagesPane extends Component {

    constructor(props) {
        super(props);
        const {match: {params: {folder}}, getMessages} = this.props;
        const unread = !(new URLSearchParams(this.props.location.search)).has('all');
        getMessages(folder, unread);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {match: {params: {folder}}, location, getMessages } = this.props;
        const unread = !(new URLSearchParams(this.props.location.search)).has('all');
        if (
            prevProps.match.params.folder !== folder ||
            prevProps.location.search !== location.search
        ) {
            getMessages(folder, unread);
        }
    }

    render() {
        const {location, match} = this.props;
        const {params: {folder}} = match;
        const unread = !(new URLSearchParams(location.search)).has('all');
        const {error, list} = this.props;


        return (
            <MesssagesListWrapper
                // footer={ markAsRead }
            >
                {folder === 'inbox' && <ul className="top-links d-flex align-items-center">
                    <li>
                        <Link
                            className={unread ? "active" : ""}
                            to={`${match.url}`}
                        >Unread</Link>
                    </li>
                    <li>
                        <Link
                            className={unread ? "" : "active"}
                            to={`${match.url}?all`}
                        >All</Link>
                    </li>
                </ul>}

                {/*<Search />*/}
                <div className="message-list">
                    <Loader loaded={true}>
                        {list && list.length > 0 && list.map(message =>
                            <MessageListItem key={message.id} {...message}/>)
                        }
                        {list && list.length === 0 && <div className='no-messages'>
                            No messages here
                        </div>}
                    </Loader>
                    {error ? <p className="error">There was a problem: {error}</p> : null}
                </div>
            </MesssagesListWrapper>
        );
    }
}

MessagesPane = connect(
    ({messages}) => messages,
    dispatch => bindActionCreators({setFolder, getMessages, refreshToken}, dispatch)
)(MessagesPane);
export default MessagesPane;
