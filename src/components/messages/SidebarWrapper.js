import React from 'react';
import { node } from 'prop-types';

function SidebarWrapper({ children }) {
  return (
    <div className="col-12 col-sm-auto sidebar">
      { children }
    </div>
  );
}

SidebarWrapper.propTypes = {
  children: node,
};

export default SidebarWrapper;
