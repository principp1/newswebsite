import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import moment from 'moment';

class MessageListItem extends Component {
    render() {
        const {subject, date, unread, draft} = this.props;

        const formattedDate = moment(date).format('D MMMM Y');
        const {id} = this.props;
        const to = draft ? `/messages/${id}/edit` : `/messages/${id}/view`;

        return (

            <Link
                to={to}
                className={`
                    message-item
                    d-flex
                    align-items-sm-center
                    justify-content-sm-between
                    flex-wrap
                    ${unread ? 'unread' : ''}
                `}>
                {/*<input*/}
                {/*className="messageToggle"*/}
                {/*type="checkbox"*/}
                {/*onChange={this.handleActiveToggle}*/}
                {/*// onClick={ this.handleToggleClick }*/}
                {/*checked={this.state.selected}*/}
                {/*/>*/}
                <span className="-subject">{subject}</span>
                <span className="-date">{formattedDate}</span>
            </Link>
        );
    }
}

export default MessageListItem;
