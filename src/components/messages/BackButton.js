import React, { Component } from 'react';
import { string } from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { linkFromPath } from './sidebarLinks';

class BackButton extends Component {
  static propTypes = {
    to: string,
    source: string,
  };

  sourceLabel(path) {
    return linkFromPath(path).label;
  }

  render() {
    const { source } = this.props;
    return (
      <Link
        to={ source }
        className="btn btn-primary btn-block compose-btn d-flex align-items-center text-left"
      >
        <i className="fa fa-angle-left" />
        { ` Back to ${this.sourceLabel(source)}` }
      </Link>
    );
  }
}

export default connect(({ messages }) => {
  return { source: messages.newMessageSource || "/messages" };
})(BackButton);
