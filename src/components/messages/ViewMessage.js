import React, {Component} from 'react';
import {getMessage} from "../../actions/messagesActions";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import rawHtml from "../../helpers/rawHtml";

import {archive, deleteMessage} from './actions';

class ViewMessage extends Component {
    constructor(props) {
        super(props);
        const {match: {params: {id}}, getMessage} = this.props;
        getMessage(id);
    }

    render() {
        const {subject, sender, message, folder} = this.props.message || {};
        const {match: {params: {id}}, deleteMessage, archive} = this.props;
        return (
            <div className="col">
                <div className="d-flex align-items-end flex-column h-100 w-100 msg-wrap">
                    <div className="w-100 mb-auto">
                        <div className="message-box h-100">
                            <ul className="top-links d-flex align-items-center">
                                {folder !== 'archive' && <li>
                                    <a
                                        href="#archive"
                                        className="svg-block"
                                        onClick={e => archive(id)}
                                    >
                                        <img src="/img/svg/noun_Inbox_In_1071659.svg" className="svg-img" alt=""/>
                                    </a>
                                </li>}

                                {folder !== 'trash' && <li>
                                    <a
                                        href="#trash"
                                        className="svg-block"
                                        onClick={e => deleteMessage(id)}
                                    >
                                        <img src="/img/svg/noun_bin_2012827.svg" className="svg-img" alt=""/>
                                    </a>
                                </li>}
                            </ul>
                            <div className="subject-block">
                                {subject}
                            </div>
                            <div className="receiver-block d-flex">
                                <span href="#" className="receiver-name">
                                    {sender && sender.name}
                                </span>
                                <span href="#" className="receiver-email">
                                    {sender && sender.email}
                                </span>
                            </div>
                            <div
                                className="read-message"
                                dangerouslySetInnerHTML={rawHtml(message)}
                            />
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

ViewMessage = connect(
    state => ({message: state.messages.message}),
    dispatch => bindActionCreators({
        getMessage,
        deleteMessage,
        archive
    }, dispatch)
)(ViewMessage);
export default ViewMessage;
