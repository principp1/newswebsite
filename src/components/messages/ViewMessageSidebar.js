import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import BackToMenuButton from './BackToMenuButton';

class ViewMessageSidebar extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
  };

  // TODO: hardcoded... need to pull from api
  getBadgeValue(id) {
    const values = {
      inbox: 7,
      trash: 9,
    };
    return values[id] || 0;
  }

  render() {
    return (
      <Fragment>
        <BackToMenuButton />

        <div className="side-nav">
          <div className="inbox-list">
            <a href="#todo" className="inbox-item active row no-gutters align-items-center">
              <div className="col-auto">
                <div className="active-dot" />
              </div>
              <div className="col">
                <div className="m_meta d-flex justify-content-between">
                  <div className="name">Name Surname</div>
                  <div className="time">12:06</div>
                </div>
                <div className="m_subject">About Issues</div>
                <div className="m_text">Lorem ipsum dolor sit amet, consectetur...</div>
              </div>
            </a>
            <a href="#todo" className="inbox-item row no-gutters align-items-center">
              <div className="col-auto">
                <div className="active-dot" />
              </div>
              <div className="col">
                <div className="m_meta d-flex justify-content-between">
                  <div className="name">Name Surname</div>
                  <div className="time">12:06</div>
                </div>
                <div className="m_subject">About Issues</div>
                <div className="m_text">Lorem ipsum dolor sit amet, consectetur...</div>
              </div>
            </a>
            <a href="#todo" className="inbox-item row no-gutters align-items-center">
              <div className="col-auto">
                <div className="active-dot" />
              </div>
              <div className="col">
                <div className="m_meta d-flex justify-content-between">
                  <div className="name">Name Surname</div>
                  <div className="time">12:06</div>
                </div>
                <div className="m_subject">About Issues</div>
                <div className="m_text">Lorem ipsum dolor sit amet, consectetur...</div>
              </div>
            </a>
            <a href="#todo" className="inbox-item row no-gutters align-items-center">
              <div className="col-auto">
                <div className="active-dot" />
              </div>
              <div className="col">
                <div className="m_meta d-flex justify-content-between">
                  <div className="name">Name Surname</div>
                  <div className="time">12:06</div>
                </div>
                <div className="m_subject">About Issues</div>
                <div className="m_text">Lorem ipsum dolor sit amet, consectetur...</div>
              </div>
            </a>
          </div>
        </div>

      </Fragment>
    );
  }
}

export default ViewMessageSidebar;

