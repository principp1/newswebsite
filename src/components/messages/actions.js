import {initialize} from 'redux-form';
import {push, client as axios, getAuthHeaders} from "../../utils";
import {FORM_NAME} from "./MessageEdit";
import {ActionTypes as types} from '../../constants';

import { show } from '../alertmodal/actions';
const errorModal = show('An error occurred while processing your request!');

export const getMessages = (folder, unread) => dispatch => {
    const params = unread ? {folder, unread} : {folder};
    return axios
        .get(`messages/`, {headers: getAuthHeaders(), params})
        .then(r => {
            dispatch(
                {
                    type: types.MESSAGES_GET_MESSAGES_SUCCESS,
                    data: r.data,
                }
            )
        })
        .catch(r => dispatch(errorModal));
};

// TODO add loader and messages
export const archive = (id, reload = '/messages/inbox') => dispatch => axios.post(
    `${process.env.REACT_APP_API_URL}/messages/${id}/archive/`, {},
    {headers: getAuthHeaders()}
)
    .then(r => dispatch(show(
        'Successfully archived!',
        push(reload)
    )))
    .catch(r => dispatch(errorModal));


export const fetchMessage = pk => dispatch => axios
    .get(`messages/${pk}/`, {headers: getAuthHeaders()})
    .then(r => dispatch(initialize(FORM_NAME, r.data)))
    .catch(r => dispatch(errorModal));



export const deleteMessage = (pk, reload = '/messages/inbox') => dispatch => axios
    .post(`messages/${pk}/trash/`, {}, {headers: getAuthHeaders()})
    .then(r => dispatch(dispatch(show('Successfully deleted!', push(reload)))))
    .catch(r => dispatch(errorModal));


