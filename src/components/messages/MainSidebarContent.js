import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';

import NewMessageButton from './NewMessageButton';
import SidebarButton from './SidebarButton';
import links from './sidebarLinks';


class MainSidebarContent extends Component {
    static propTypes = {
        children: PropTypes.node,
        className: PropTypes.string,
    };

    // TODO: hardcoded... need to pull from api
    getBadgeValue(id) {
        return 0;
    }

    render() {
        const { match: {params: {folder}}} = this.props;
        return (
            <Fragment>
                <NewMessageButton
                    to="/messages/new"
                    source={this.props.location.pathname}
                />
                <div className="side-nav">
                    {links.map((link, index) => <div
                        className="list-group"
                        key={link.id}>
                            <SidebarButton
                                to={link.to}
                                id={link.id}
                                active={folder === link.id}
                                label={link.label}
                                icon={<link.icon/>}
                                badgeValue={this.getBadgeValue(link.id)}
                            />
                        </div>
                    )}
                </div>
            </Fragment>
        );
    }
}

// MainSidebarContent = connect(state => ({folder: state.messages.folder}))(MainSidebarContent);

export default MainSidebarContent;
