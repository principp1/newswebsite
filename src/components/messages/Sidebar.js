import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Redirect, Route, Switch} from 'react-router-dom';

import SidebarWrapper from './SidebarWrapper';
import BackButton from './BackButton';
import MainSidebarContent from './MainSidebarContent';
import ViewMessagesSidebar from './ViewMessageSidebar';

class Sidebar extends Component {
    static propTypes = {
        children: PropTypes.node,
        className: PropTypes.string,
    };

    render() {
        return (
            <SidebarWrapper>
                <Switch>
                    <Route
                        exact
                        path="/messages"
                        render={() => <Redirect to="/messages/inbox"/>}
                    />
                    <Route
                        path="/messages/new"
                        render={() => <BackButton/>}
                    />
                    <Route
                        path="/messages/view"
                        render={() => <ViewMessagesSidebar/>}
                    />
                    <Route
                        path='/messages/:folder'
                        component={MainSidebarContent}
                    />
                </Switch>
            </SidebarWrapper>
        );
    }
}

export default Sidebar;
