import React from 'react';
import { node } from 'prop-types';

function MesssagesListWrapper({ children, footer }) {
  return (
    <div className="col">
      <div className="d-flex align-items-end flex-column h-100 w-100 msg-wrap">
        <div className="w-100 mb-auto">
          <div className="message-box h-100">
            { children }
          </div>
        </div>
        <div className="w-100 foot-padding">
          { footer }
        </div>
      </div>
    </div>

  );
}

MesssagesListWrapper.propTypes = {
  chilren: node,
  footer: node
};

export default MesssagesListWrapper;
