import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import Loader from '../../elements/Loader';
import If from '../../elements/If';
import {getPosts} from '../../actions/blogActions';
import MainLayout from '../layout/MainLayout';
import Banner from './Banner';
import ArticlesGrid from './ArticlesGrid';

import './MainPage.scss';
import {Helmet} from "react-helmet";
import Header from "../Header";
class MainPage extends Component {
    static propTypes = {
        children: PropTypes.node,
        className: PropTypes.string,
    };

    componentDidMount() {
        const {sequence} = this.props;
        if (sequence.length === 0) {
            this.getTheNextPosts();
        }
    }

    getThePosts(params) {
        this.props.dispatch(getPosts(params));
    }

    getTheNextPosts() {
        const {posts, page} = this.props;
        if (posts) {
            const url = new URL(posts.next);
            const startAt = url.searchParams.get('before_of');
            this.getThePosts({page: page + 1, startAt, pageSize: 10});
        } else {
            this.getThePosts({page, pageSize: 7});
        }
    }

    renderBottomLink() {
        const {posts} = this.props;
        if (posts && posts.next) {
            return (
                <button
                    onClick={() => this.getTheNextPosts()}
                    className="button btn btn-primary btn-read-more"
                >Read More Stories</button>
            )
        }
    }

    render() {
        const {page, sequence, cache, postsLoaded, error} = this.props;
        let posts = sequence.map(slug => cache[slug]);
        const gridProps = {posts, page, error};
        return (
            <MainLayout bodyClass="news-page news-home">
                <Helmet>
                    <title>News</title>
                </Helmet>
                <Banner post={posts} />
                <ArticlesGrid
                    postsLoaded={sequence.length > 0}
                    {...gridProps}
                />
                <If condition={sequence.length > 0}>
                    <div className="next-loader">
                        <Loader loaded={postsLoaded}>
                            <div className="news-bottom">
                                <div className="row">
                                    {this.renderBottomLink()}
                                </div>
                            </div>
                        </Loader>
                    </div>
                </If>
            </MainLayout>
        );
    }
}

export default connect(({blog}) => {
    const {
        page, pageSize, posts,
        postsCache,
        postsLoaded,
        postsSequence,
        error
    } = blog;
    return {
        page, pageSize, posts,
        postsLoaded, error,
        sequence: postsSequence,
        cache: postsCache,
    };
})(MainPage);
