import React from 'react';
import { Link } from 'react-router-dom';
import summarize from '../../helpers/summarize';
import dateFormat from '../../helpers/dateFormat';
import articlePath from './articlePath';
import Author from './Author';
import './GridItem.scss';

function clickable(e) {
  if (e.target.tagName !== 'A') {
    const container = e.currentTarget;
    container.getElementsByTagName('a')[0].click();
  }
}
function Banner({...props}) {
  const posts = props.post;
  let postdata = []
  let article = null;
  posts.map((article) => {
    if (article.featured == true) {
      postdata.push(article);
    }
    
  })
  if (postdata) {
    article = postdata[0];
  }

  const sizeClass = '-small';
  const metaClass = 'd-flex';
  const topicClass = 'mb-1';
  const topicEl = (
    <div className={`meta-text ${topicClass}`}>{article && article.topic.name}</div>
  );

  return (<div>
   {!article ? 
    <div className="page_header d-flex align-items-center set-height">
      <div className="wrap -small">
      </div>
    </div>: 
   <div className="page_header d-flex align-items-center set-height" style={{
    backgroundImage: `url(${article && article.thumbnail})`
  }}>
   <div className="wrap news-grid set-grid" style={{"backgroundColor":"transparent"}}>
     {/* <h1 className="page_title">News</h1> */}
     <div style={{"backgroundColor":"transparent"}}
       className={`grid-item closeHover  grid-item-article set-size ${sizeClass}`}
       onClick={clickable}
     >
       <div className="g-content d-flex align-items-end">
         <div className="post-box">
           <div className={`post-meta ${metaClass}`}>
             {topicEl }
             <div className="meta-date">{dateFormat(article && article.created_at)}</div>
             {null}
           </div>
           <Link to={articlePath(article && article.slug)} className="post-title">{article && article.title}</Link>
           <p>{summarize(article && article.content, 15)}…</p>
           <Author author={article && article.author} readMinutes={article && article.read_in} />
         </div>
       </div>
     </div>

   </div>
 </div>
  }
  </div>
  );
}

export default Banner;
