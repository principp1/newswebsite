import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Loader from '../../elements/Loader';
import GridItem from './GridItem';

class ArticlesGrid extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
  };

  renderPosts() {
    const { posts } = this.props;
    let postdata = []
    posts.map((article) => {
      if (article.featured === false) {
        postdata.push(article);
      }

    })
    return postdata.map((post, i) => (
      // <div key={post.slug} className={`col-6 ${i > 0 ? 'col-md-6' : ''}`}>
      <div key={post.slug} className={`col-md-6`}>
        <GridItem article={post} />
      </div>
    ))
  }

  render() {
    const { postsLoaded } = this.props;
    return (
      <div className="news-grid">
        <div className="wrap -small">
          <div className="row">
            <Loader loaded={postsLoaded}>{() => this.renderPosts()}</Loader>
          </div>
        </div>
      </div>
    );
  }
}

export default ArticlesGrid;
