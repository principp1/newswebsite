import React from 'react';
import { articleAuthor } from '../../propTypes';
import { number } from 'prop-types';

function Author({ author, readMinutes }) {
  return (
    <div className="autor-box d-flex align-items-center">
      <div className="author-img">
        <img src={ author && author.photo } alt="avatar" />
      </div>
      <div className="author-details">
        <div className="name">{ author && author.name }</div>
        <div className="time">{ readMinutes } min. Read</div>
      </div>
    </div>
  );
}

Author.propTypes = {
  author: articleAuthor.isRequired,
  readMinutes: number
};

export default Author;
