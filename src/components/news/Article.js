import React, { Component } from 'react';

import rawHtml from '../../helpers/rawHtml';
import dateFormat from '../../helpers/dateFormat';
import { article } from '../../propTypes';
import If from '../../elements/If';
import { Link } from 'react-router-dom';
import AuthorBox from './AuthorBox';

import './Article.scss';

class Article extends Component {
  static propTypes = {
    article
  };

  render() {
    const {
      title, content, created_at, thumbnail, author, topic, next, previous
    } = this.props.article;

    return (
      <div className="news-wrap">
        <article className="post-entry">
          <h1 className="post-heading">{ title }</h1>
          <div className="post-meta d-flex">
            <span className="meta-date">{ dateFormat(created_at) }</span>
            <span className="meta-text ml-3">{ topic.name }</span>
          </div>
          <img src={ thumbnail } alt="post" className="post-image img-fluid" />
          <div dangerouslySetInnerHTML={ rawHtml( content ) } />

          <div className="post-nav">
            <div className="news-wrap">
              
              <If condition={ next }>
                <Link
                  to={ `/news/${ next }` }
                  title="Next Article"
                  className="c_control c-control-prev"
                  role="button"
                  data-slide="prev"
                >
                  <i className="fa fa-angle-left" />
                  <span className="sr-only">Next</span>
                </Link>
              </If>
              <If condition={ previous }>
                <Link
                  to={ `/news/${ previous }` }
                  title="Previous Article"
                  className="c_control c-control-next"
                  role="button"
                  data-slide="next"
                >
                  <i className="fa fa-angle-right" />
                  <span className="sr-only">Previous</span>
                </Link>
              </If>
            </div>
          </div>

          <AuthorBox author={ author } />
        </article>
      </div>

    );
  }
}

export default Article;
