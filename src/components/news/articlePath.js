export default function articlePath(slug) {
  return `/news/${slug}`;
}

