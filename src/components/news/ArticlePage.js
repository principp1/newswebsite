import React, {Component, Fragment} from 'react';
import {bool} from 'prop-types';
import {connect} from 'react-redux';

import {article} from '../../propTypes';
import Loader from '../../elements/Loader';
import MainLayout from '../layout/MainLayout';
import Article from './Article';
import RecentPosts from './RecentPosts';
import {getPost, getRecentPosts} from '../../actions/blogActions';
import {Helmet} from "react-helmet";

class ArticlePage extends Component {
    static propTypes = {
        article,
        loaded: bool
    };

    constructor(props) {
        super(props);
        this.renderArticle = this.renderArticle.bind(this);
    }

    renderArticle() {
        const {post, recentPosts} = this.props;
        return (
            <Fragment>
                <Article article={post}/>
                {recentPosts ? <RecentPosts posts={recentPosts.results}/> : null}
            </Fragment>
        );
    }

    requestData(slug) {
        const {dispatch} = this.props;
        dispatch(getPost(slug));
        dispatch(getRecentPosts());
    }

    componentDidMount(prev) {
        this.requestData(this.props.match.params.slug)
    }

    componentDidUpdate(prev) {
        const {match} = this.props;
        const {slug} = match.params;
        if (prev.match && prev.match.params.slug !== slug) {
            this.requestData(slug)
        }
    }

    render() {
        const {post, error, loaded} = this.props;

        return (
            <MainLayout bodyClass="news-page news-single">
                <Helmet>
                    {post ?
                        <title>News - {post.title}</title>
                        : <title>News</title>
                    }
                </Helmet>
                <Loader loaded={loaded}>{() => post ? this.renderArticle() : null}</Loader>
                {error ? <p className="error">There was a problem: {error.message}</p> : null}
            </MainLayout>
        );
    }
}

export default connect(({blog}) => {
    const {post, error, postLoaded, recentPosts, recentPostsLoaded} = blog;
    return {post, error, loaded: postLoaded, recentPosts, recentPostsLoaded};
})(ArticlePage);
