import React from 'react';
import { bool } from 'prop-types';
import { Link } from 'react-router-dom';

import { article } from '../../propTypes';
import summarize from '../../helpers/summarize';
import dateFormat from '../../helpers/dateFormat';
import articlePath from './articlePath';
import Author from './Author';

import './GridItem.scss';

function clickable(e) {
  if (e.target.tagName !== 'A') {
    const container = e.currentTarget;
    container.getElementsByTagName('a')[0].click();
  }
}

function GridItem({ big, article }) {
  const sizeClass = '-small';
  const metaClass = '';
  const topicClass = 'mb-1';
  const topicEl = (
    <div className={`meta-text ${topicClass}`}>{article.topic.name}</div>
  );
  return (
    <div
      className={`grid-item grid-item-article ${sizeClass}`}
      onClick={clickable}
      style={{
        backgroundImage: `url(${article.thumbnail})`
      }}
    >
      <div className="g-content d-flex align-items-end">
        <div className="post-box">
          <div className={`post-meta ${metaClass}`}>
            { topicEl}
            <div className="meta-date">{dateFormat(article.created_at)}</div>
            {null}
          </div>
          <Link to={articlePath(article.slug)} className="post-title">{article.title}</Link>
          <p>{summarize(article.content, 15)}…</p>
          <Author author={article.author} readMinutes={article.read_in} />
        </div>
      </div>
    </div>
  );
}

GridItem.propTypes = {
  big: bool,
  article
};

export default GridItem;
