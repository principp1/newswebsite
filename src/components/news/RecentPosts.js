import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { arrayOf } from 'prop-types';

import dateFormat from '../../helpers/dateFormat';
import articlePath from './articlePath';
import { article } from '../../propTypes';


class RecentPosts extends Component {
  static propTypes = {
    posts: arrayOf(article),
  };

  recentItem(article) {
    return (
      <div className="col-12 col-sm-6 col-xl-3" key={ article.slug }>
        <div className="recent-post">
          <div className="post-meta">{ dateFormat(article.created_at) }</div>
          <Link to={ articlePath(article.slug) } className="post-title">{ article.title }</Link>
          <div className="post-cat">{ article.topic.name }</div>
          <div className="post-author">{ article.author.name }</div>
          <div className="post-time">{ article.read_in } min. Read</div>
        </div>
      </div>
    );
  }

  renderArticles() {
    const { posts } = this.props;
    return posts.map(article => this.recentItem(article));
  }

  render() {
    return (
      <div className="wrap">
        <div className="recent-posts-widget">
          <h1 className="widget-header">Recent posts</h1>
          <div className="row">
            { this.renderArticles() }
          </div>
        </div>
      </div>
    );
  }
}

export default RecentPosts;
