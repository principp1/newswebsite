import React from 'react';
import { articleAuthor } from '../../propTypes';

import './AuthorBox.scss';

function AuthorBox({ author }) {
  return (
    <div className="post-author-box">
      <div className="row">
        <div className="col-12 col-md-auto">
          <img src={ author.photo } className="avatar-img" alt="avatar" />
        </div>
        <div className="col">
          <div className="author-name">{ author.name }</div>
          <div className="author-bio">{ author.bio }</div>
        </div>
      </div>
    </div>
  );
}

AuthorBox.propTypes = {
  author: articleAuthor.isRequired,
};

export default AuthorBox;

