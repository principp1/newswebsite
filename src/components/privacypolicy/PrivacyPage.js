import React from 'react';

import MainLayout from '../layout/MainLayout';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {fetch} from './actions';
import '../PrivacyPage.scss';
import rawHtml from "../../helpers/rawHtml";
import {Helmet} from 'react-helmet';

class PrivacyPage extends React.Component{
    constructor(props){
        super(props);
        const { fetch } = this.props;
        fetch();
    }
    render(){
        const { content, title } = this.props;
        return (
            <MainLayout bodyClass="privacy-policy">
                <Helmet>
                    <title>Privacy Page</title>
                </Helmet>
                <div className="page-header">
                    <div className="wrap h-100">
                        <div className="row h-100 align-items-center">
                            <div className="col-12">
                                <h1 className="page-title">{title}</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="page-content">
                    <div className="wrap" dangerouslySetInnerHTML={ rawHtml( content ) }>

                    </div>
                </div>
            </MainLayout>
        );
    }
}

PrivacyPage = connect(
    state => ({ ...state.layout.privacyPolicy }),
    dispatch => bindActionCreators({
        fetch
    }, dispatch)
)(PrivacyPage);
export default PrivacyPage;
