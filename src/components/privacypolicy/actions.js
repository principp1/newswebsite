
import {client as axios, getAuthHeaders} from "../../utils";

export const fetch = () => dispatch => axios.get('cms/privacy-policy/', {headers: getAuthHeaders()})
    .then(r => dispatch({type: '@@cms/privacy-policy/FETCH', payload: r.data}));