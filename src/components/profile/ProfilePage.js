import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import MainLayout from '../layout/MainLayout';
import ProfileWrap from './ProfileWrap';
import {fetch, update} from './actions';
import './ProfilePage.scss';
import {ProfileForm} from "./forms";
import {Helmet} from "react-helmet";

class ProfilePage extends Component {
    constructor(props) {
        super(props);
        const {fetch} = this.props;
        fetch();
    }

    render() {
        const {update} = this.props;
        return (
            <MainLayout bodyClass="profile-page">
                <Helmet>
                    <title>Profile</title>
                </Helmet>
                <ProfileWrap className="edit-profile">
                    <ProfileForm
                        onSubmit={data => update(data)}/>
                </ProfileWrap>
            </MainLayout>
        );
    }
}

ProfilePage = connect(
    state => ({}),
    dispatch => bindActionCreators({fetch, update}, dispatch)
)(ProfilePage);
export default ProfilePage;
