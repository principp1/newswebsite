import React from 'react';
import { string, node } from 'prop-types';

function ProfileWrap({ className, children }) {
  return (
    <section className={ className }>
      <div className="wrap">{ children }</div>
    </section>
  );
}

ProfileWrap.propTypes = {
  className: string,
  children: node.isRequired
};

export default ProfileWrap;
