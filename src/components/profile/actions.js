import {initialize, SubmissionError} from 'redux-form';
import {FORM_NAME} from './forms';
import {client, getAuthHeaders} from "../../utils";
// import tokenProvider from "axios-token-interceptor";


// this.client.interceptors.request.use(tokenProvider({
//   getToken: () => accessToken
// }));

const fetch = () => dispatch => client
    .get('employee/fetch/', {headers: getAuthHeaders()})
    .then(r => dispatch(initialize(FORM_NAME, r.data)))

const update = data => dispatch => {

    const form = new FormData();

    Object.keys(data).map(k => {
        const value = data[k];
        if (k === 'photo' && !(value instanceof File)) {
            return k;
        }
        form.append(k, data[k]);
        return k;
    });

    return client
        .patch('employee/update/', form, {
            headers: {'Content-Type': 'multipart/form-data', ...getAuthHeaders()}
        })
        .then(r => {
            window.location.reload();
        })
        .catch(r => {
            throw new SubmissionError({
                ...(r.response ? r.response.data : {}),
                _error: 'Please provide a valid email address'
            })
        });
};

export {fetch, update}