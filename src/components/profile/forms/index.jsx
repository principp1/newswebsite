import React from 'react';

import {Field, reduxForm} from 'redux-form';
import {Checkbox, ImageInput, Input, Password} from "../../forms/fields";
import Row from "../../../elements/grid/Row";

export const FORM_NAME = 'profile-form';

class ProfileForm extends React.Component {

    render() {
        const {handleSubmit, submitting } = this.props;
        return <form onSubmit={handleSubmit} className="profile-form">
            {/*{error && <div className="logout-alert">{error}</div>}*/}

            <h1 className="profile-heading">Edit Your Profile</h1>

            <Row>
                <div className="col-lg-4">
                    <div className="side-bar bg-trans">
                        <h2>Upload your photo</h2>
                        <Field
                            disabled={submitting}
                            name='photo'
                            component={ImageInput}
                        />
                        <div className="clearfix"/>
                    </div>
                </div>
                <div className="col-lg-8">
                    <div className="form-wrap bg-trans">
                        <h2 className="form-title">About you</h2>
                        <div className="row mb-60">
                            <div className="col-12 col-md-6">
                                <Field
                                    disabled={submitting}
                                    name={'firstName'}
                                    label='First name'
                                    placeholder='Johathan'
                                    component={Input}
                                />
                            </div>
                            <div className="col-12 col-md-6">
                                <Field
                                    disabled={submitting}
                                    name={'lastName'}
                                    label='Last name'
                                    placeholder='White'
                                    component={Input}
                                />
                            </div>
                            <div className="col-12">
                                <Field
                                    disabled={submitting}
                                    name={'address'}
                                    label='Address'
                                    placeholder="438 Rathdowne Road Carlton, Victoria 3001"
                                    component={Input}
                                />
                            </div>
                            <div className="col-12 col-md-6">
                                <Field
                                    disabled={submitting}
                                    name='city'
                                    label='City'
                                    placeholder="London"
                                    component={Input}
                                />
                            </div>
                            <div className="col-12 col-md-6">
                                <Field
                                    disabled={submitting}
                                    name='postCode'
                                    label='Post code'
                                    placeholder="900556"
                                    component={Input}
                                />
                            </div>
                            <div className="col-12">
                                <Field
                                    disabled={submitting}
                                    name='telephone'
                                    label='Telephone number'
                                    placeholder="+49 0554 0444 11"
                                    component={Input}
                                />
                            </div>
                        </div>
                        <h2 className="form-title">Profile Settings</h2>
                        <div className="row">
                            <div className="col-12">
                                <Field
                                    disabled={submitting}
                                    name='password'
                                    label='Password'
                                    placeholder="********"
                                    component={Password}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <Field
                                    disabled={submitting}
                                    name='password_confirm'
                                    label='Confirm password'
                                    placeholder="********"
                                    component={Password}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <Field
                                    disabled={submitting}
                                    name='email_notification_enabled'
                                    label='Enable e-mail notifications'
                                    component={Checkbox}
                                />
                            </div>
                        </div>
                        {/*<h2 className="form-title">Google Multifactor Authentication</h2>*/}
                        <div className="row">
                            <div className="col-12">
                                <Field
                                    disabled={submitting}
                                    name='google_mfa_enabled'
                                    component={Checkbox}
                                    label={'Enable Google Multifactor Authentication'}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </Row>
            <div className="text-center text-md-right">
                <button
                    disabled={submitting}
                    type="submit"
                    className="btn btn-primary rounded-0 submit-btn">Update</button>
            </div>

        </form>
    }
}

ProfileForm = reduxForm({form: FORM_NAME})(ProfileForm);
export {ProfileForm};