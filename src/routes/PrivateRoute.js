import React from 'react';
import {Redirect, Route, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {refreshToken} from "../actions/authenticationActions";


class PrivateRoute extends React.Component {

    render() {
        const {component: Component, isLoggedIn, ...rest} = this.props;
        return <Route
            {...rest}
            render={props => isLoggedIn ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/login",
                        state: {from: props.location}
                    }}
                />
            )
            }
        />
    }
}

PrivateRoute = connect(
    ({authentication: {auth}}) => ({auth}),
    dispatch => bindActionCreators({refreshToken}, dispatch)
)(PrivateRoute);
export default withRouter(PrivateRoute);
