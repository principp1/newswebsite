import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {ScrollContext} from 'react-router-scroll-4';

import AppRedirect from './AppRedirect';
import PrivateRoute from './PrivateRoute';
import Documents from '../containers/Documents';
import Benefits from '../containers/Benefits';
import Login from '../containers/Login';
import ForgotPassword from '../pages/auth/ForgotPassword';
import ForgotPasswordConfirm from '../pages/auth/ForgotPasswordConfirm';
import NotFound from '../containers/NotFound';
import Profile from '../containers/Profile';
import News from '../containers/News';
import PrivacyPage from '../components/privacypolicy/PrivacyPage';

import Tools from '../components/Tools';
import axios from 'axios';
import {client, requestInterceptor, responseInterceptor} from "../utils";
import MessagesPage from "../components/messages/MessagesPage";
import HomePage from "../components/homepages/HomePage";

// Add a request interceptor
axios.interceptors.request.use(requestInterceptor, responseInterceptor);
client.interceptors.request.use(requestInterceptor, responseInterceptor);

const privateRoutes = [
    {path: "/", component: HomePage, exact: true},
    {path: "/wealth", component: Benefits},
    {path: "/health", component: Benefits},
    {path: "/lifestyle", component: Benefits},
    {path: "/news", component: News},
    {path: "/messages", component: MessagesPage},
    {path: "/documents", component: Documents},
    {path: "/profile", component: Profile},
    {path: "/privacy-policy", component: PrivacyPage},
];

class Routing extends Component {
    static propTypes = {
        children: PropTypes.node,
        className: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.privateRoute = this.privateRoute.bind(this);
    }

    privateRoute(props) {
        const {isLoggedIn} = this.props;
        return (<PrivateRoute key={props.path} isLoggedIn={isLoggedIn} {...props} />);
    }

    renderPrivateRoutes() {
        return privateRoutes.map(this.privateRoute);
    }

    render() {
        return (
            <Router>
                <ScrollContext
                    shouldUpdateScroll={(prev, {location, history}) => (
                        prev && !(
                            prev.location.pathname.match(/^\/messages/) &&
                            location.pathname.match(/^\/messages/)
                        )
                    )}
                >
                    <Fragment>
                        <Switch>
                            {this.renderPrivateRoutes()}

                            <Route path="/login" component={Login}/>
                            <Route path="/forgot-password" component={ForgotPassword}/>
                            <Route path="/reset-password/:uuid" component={ForgotPasswordConfirm}/>
                            <Route
                                path="/create-password/:uuid"
                                component={ForgotPasswordConfirm}
                            />
                            <Route path="/tools" component={Tools}/>

                            <Route component={NotFound}/>
                        </Switch>
                        <AppRedirect/>

                    </Fragment>
                </ScrollContext>
            </Router>
        );
    }
}

export default connect(({authentication}) => {
    const {isLoggedIn} = authentication;
    return {isLoggedIn};
})(Routing);
