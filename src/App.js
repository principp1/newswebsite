import React, {Component} from 'react';
import {connect} from 'react-redux';

import Routing from './routes/Routing';

import './App.scss';
import {api} from './services/api';
import {loggedOut} from './actions/authenticationActions';

import ReduxSweetAlert from 'react-redux-sweetalert';
import './sweetalert.css';

class App extends Component {
    componentDidMount() {
        api.onUnauthorized(() => {
            this.props.dispatch(loggedOut());
        });
    }

    render() {
        return (
            <div className="App">
                <Routing/>
                <ReduxSweetAlert/>
            </div>
        );
    }
}

export default connect(state => {
    return {};
})(App);
