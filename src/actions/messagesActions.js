import {ActionTypes as types} from '../constants';
import action from '../helpers/action';
import {client, getAuthHeaders} from "../utils";

export function newMessage(data) {
    return action(types.MESSAGES_NEW_MESSAGE, data);
}

export function getMessages(folder, options = {}) {
    return action(types.MESSAGES_GET_MESSAGES, {folder, ...options});
}

export function getMessage(id) {
    return action(types.MESSAGES_GET_MESSAGE, {id})
}

export function loadMessages(messages) {
    return action(types.MESSAGES_GET_MESSAGES_SUCCESS, messages);
}

export function loadMessage(message) {
    return action(types.MESSAGES_GET_MESSAGE_SUCCESS, message);
}

export function getMessagesError(e) {
    return action(types.MESSAGES_GET_MESSAGES_FAILURE, e);
}


export const fetchRecipients = (q = '') => dispatch =>
    client.get(`${process.env.REACT_APP_API_URL}/messages/recipients/`, {
        params: { q },
        headers: getAuthHeaders()
    })
        .then(r => dispatch({
            type: '@@message/RECIPIENTS',
            data: r.data
        }));


export const setFolder = folder => ({type: 'MESSAGES_SET_FOLDER', data: folder});