import { ActionTypes as types } from '../constants';
import action from '../helpers/action';

export function getPosts(params) {
  return action(types.BLOG_GET_POSTS, params);
}

export function loadPosts(posts) {
  return action(types.BLOG_GET_POSTS_SUCCESS, posts);
}

export function getPostsFail(error) {
  return action(types.BLOG_GET_POSTS_FAILURE, error);
}

export function getPost(slug) {
  return action(types.BLOG_GET_POST, slug);
}

export function loadPost(post) {
  return action(types.BLOG_GET_POST_SUCCESS, post);
}

export function getPostFail(error) {
  return action(types.BLOG_GET_POST_FAILURE, error);
}

export function getRecentPosts(params) {
  return action(types.BLOG_GET_RECENT_POSTS, params);
}

export function loadRecentPosts(posts) {
  return action(types.BLOG_GET_RECENT_POSTS_SUCCESS, posts);
}

export function getRecentPostsFail(error) {
  return action(types.BLOG_GET_RECENT_POSTS_FAILURE, error);
}
