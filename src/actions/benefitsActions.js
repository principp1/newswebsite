import { ActionTypes as types } from '../constants';
import action from '../helpers/action';

export function getBenefit(data) {
  return action(types.BENEFITS_GET_BENEFIT, data);
}

export function loadBenefit(pageData) {
  return action(types.BENEFITS_GET_BENEFIT_SUCCESS, pageData);
}

export function getBenefitFailure(error) {
  return action(types.BENEFITS_GET_BENEFIT_FAILURE, error);
}

