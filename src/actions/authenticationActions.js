import {ActionTypes as types} from '../constants';
import action from '../helpers/action';
import axios from 'axios';
import oauth from 'axios-oauth-client';
import moment from "moment-timezone";
import {getRefreshToken, SESSION_KEY} from "../utils";


export function login(data) {
    return action(types.AUTHENTICATION_LOGIN, data);
}

export function loginSuccess(data) {
    return action(types.AUTHENTICATION_LOGIN_SUCCESS, data);
}

export function mfaChallenge(data) {
    return action(types.AUTHENTICATION_MFA_CHALLENGE, data);
}

export function mfaChangeOTP(data) {
    return action(types.AUTHENTICATION_MFA_CHANGE_OTP, data);
}

export function mfaConfirm(data) {
    return action(types.AUTHENTICATION_MFA_CONFIRM, data);
}

export function loginFailure(e) {
    return action(types.AUTHENTICATION_LOGIN_FAILURE, e);
}

export function loggedIn() {
    return action(types.AUTHENTICATION_LOGGED_IN);
}

export function loggedOut() {
    return action(types.AUTHENTICATION_LOGGED_OUT);
}

export function logout() {
    return action(types.AUTHENTICATION_LOGOUT);
}

export function clearLogoutSuccessMessage() {
    return action(types.AUTHENTICATION_LOGOUT_CLEAR);
}

export function clearLoginFailureMessage() {
    return action(types.AUTHENTICATION_LOGIN_FAILURE_CLEAR);
}


export const refreshToken = () => dispatch => {
    const refreshToken = getRefreshToken();
    if (!refreshToken) {
        return dispatch(logout());
    }
    const authClient = axios.create({
        auth: {
            username: process.env.REACT_APP_CLIENT_ID,
            password: process.env.REACT_APP_CLIENT_SECRET,
        },
    });
    return oauth.client(authClient, {
        url: `${process.env.REACT_APP_API_URL}/authorize/`,
        grant_type: 'refresh_token',
        refresh_token: refreshToken,
    })()
        .then(data => {
            data.expires = moment().add(data.expires_in, 'seconds').valueOf();
            window.localStorage.setItem(SESSION_KEY, JSON.stringify(data));
            dispatch({type: types.AUTHENTICATION_SET_TOKEN, data})
        })
        .catch(r => {
            dispatch(logout());
        });
};
// axios.post(
//     `${process.env.REACT_APP_API_URL}/token/`,
//     {
//         grant_type: 'refresh_token',
//         refresh_token: refreshToken
//     }, {
//         Authorization: `Basic ${process.env.REACT_APP_CLIENT_ID}`,
//         'content-type': 'application/x-www-form-urlencoded'
//     })
//         .then(r => window.localStorage.setItem(SESSION_KEY', r.data))
// )