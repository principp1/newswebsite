import {
  arrayOf,
  bool,
  checkPropTypes,
  instanceOf,
  number,
  object,
  shape,
  string
} from 'prop-types';

export const benefitPage = shape({
  type: string.isRequired,
  slug: string.isRequired,
  data: object.isRequired
})

export const benefitsState = shape({
  benefitPage
});

export const linkData = shape({
  text: string.isRequired,
  path: string.isRequired,
});

export const arrayOfLinks = arrayOf(linkData);

export const navDropDownData = shape({
  parent: linkData.isRequired,
  children: arrayOfLinks.isRequired,
});

export const accordionSection = shape({
  title: string.isRequired,
  content: string.isRequired,
});

export const accordionData = arrayOf(accordionSection);

const messageBase = {
  id: string.isRequired,
  subject: string.isRequired,
  date: string.isRequired,
  unread: bool.isRequired,
};

export const messagesListItem = shape(messageBase);

export const messagesList = arrayOf(messagesListItem);

export const articleAuthor = shape({
  name: string.isRequired,
  bio: string,
  photo: string,
});

export const articleTopic = shape({
  name: string.isRequired,
  slug: string.isRequired,
});

export const article = shape({
  id: number.isRequired,
  topic: articleTopic.isRequired,
  author: articleAuthor.isRequired,
  created_at: string.isRequired,
  updated_at: string.isRequired,
  title: string.isRequired,
  thumbnail: string.isRequired,
  read_in: number,
  content: string.isRequired,
});

export const posts = shape({
  count: number,
  next: string,
  previous: string,
  results: arrayOf(article)
});

export const blogState = shape({
  posts,
  pageSize: number.isRequired,
  page: number.isRequired,
  postsLoaded: bool.isRequired,
  error: instanceOf(Error),
  post: article,
  postLoaded: bool.isRequired
});

function watchConsole(fn) {
  const oldConsole = console.error;
  let message;
  console.error = msg => (message = msg);
  fn();
  console.error = oldConsole;
  return message;
}



export const validateReducer = (shape, reducerName) => reducer => {
  const env = process.env.NODE_ENV;

  if (env === 'development' || env === 'test') {
    return (state, action) => {
      const newState = reducer(state, action);
      const getStack = () => Error().stack;
      const result = watchConsole(() =>
        checkPropTypes(
          { state: shape },
          { state: newState },
          'property',
          reducerName,
          getStack
        )
      );
      if (result) {
        console.error(`${reducerName} Failed on action: `, action, result);
      }
      return newState;
    };
  }
  return reducer;
};
