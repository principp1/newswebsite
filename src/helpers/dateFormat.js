import moment from 'moment';

export default function dateFormat(strDate) {
  return moment(strDate).format('D MMMM, Y');
}
