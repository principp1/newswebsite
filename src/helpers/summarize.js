function toText(htmlString) {
  const tmp = document.createElement('div');
  tmp.innerHTML = htmlString;
  return tmp.innerText;
}

export default function summarizeFoo(content, wordLength) {
  const text = toText(content);

  return text.split(/\s/g).slice(0, wordLength).join(" ");
}
