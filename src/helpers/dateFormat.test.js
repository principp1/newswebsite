import dateFormat from './dateFormat';

describe('dateFormat', () => {
  const testData = {
    "2018-11-06T10:00:44.076236Z": "6 November, 2018",
    "2000-03-23T10:00:44.076236Z": "23 March, 2000",
  };

  for (let raw in testData) {
    it(`handles ${raw}`, () => {
      expect(dateFormat(raw)).toEqual(testData[raw]);
    });
  }
});
