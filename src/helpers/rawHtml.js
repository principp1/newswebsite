export default function rawHtml(raw) {
  return { __html: raw };
}
