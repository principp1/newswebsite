import { all } from 'redux-saga/effects';

import authentication from './authenticationSagas';
import benefits from './benefitsSagas';
import blog from './blogSagas';
import messages from './messages';

export default function* rootSaga() {
  yield all([
    authentication(),
    benefits(),
    blog(),
    messages(),
  ]);
}
