import {all, put, takeLatest,} from 'redux-saga/effects';
import {ActionTypes as types} from '../constants';
import {getMessagesError, loadMessage, loadMessages} from '../actions/messagesActions';
import {api} from '../services/api';

function* watchNewMessage() {
    yield takeLatest(types.MESSAGES_NEW_MESSAGE, newMessage);
}

function* newMessage({type, data}) {
    try {
        yield api.newMessage(data);
        window.location.href = '/messages/inbox';
    } catch (e) {
        yield put(getMessagesError(e));
    }
}

function* watchGetMessages() {
    yield takeLatest(types.MESSAGES_GET_MESSAGES, getMessages);
}

function* getMessages(action) {
    const {folder, unread} = action.data;
    try {
        const messages = yield api.getMessages(folder, {unread});
        yield put(loadMessages(messages ? messages.data.results : []));
    } catch (e) {
        yield put(getMessagesError(e));
    }
}

function* watchGetMessage() {
    yield takeLatest(types.MESSAGES_GET_MESSAGE, fetchMessage);
}

function* fetchMessage({type, data: {id}}) {
    try {
        const message = yield api.getMessage(id)
        yield put(loadMessage(message));
    } catch (e) {
        yield put(getMessagesError(e));
    }
}

export default function* () {
    yield all([
        watchGetMessages(),
        watchGetMessage(),
        watchNewMessage(),
    ]);
}

