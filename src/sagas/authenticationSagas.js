import {
  all,
  put,
  takeLatest,
} from 'redux-saga/effects';
import { ActionTypes as types } from '../constants';
import {
    loginSuccess,
    loginFailure,
    loggedIn,
    mfaChallenge,
} from '../actions/authenticationActions';
import { api } from '../services/api';

function* load() {
  if (yield api.isLoggedIn()) {
    yield put(loggedIn());
  }
}

function* watchLogin() {
  yield takeLatest(types.AUTHENTICATION_LOGIN, authenticate);
}

function* watchMFAConfirm() {
  yield takeLatest(types.AUTHENTICATION_MFA_CONFIRM, mfaConfirmSaga);
}

function* mfaConfirmSaga(action){
  const { data } = action;
  try {
    yield api.mfaConfirm(data);
    yield put(loginSuccess({ redirectTo: '/' }));
  }catch(e){
    yield put(loginFailure(e));
  }
}

function* authenticate(action) {
  const { data } = action;
  try {
    const mfaConfig = yield api.authenticate(data);
    if (!mfaConfig){
      yield put(loginSuccess({ redirectTo: data.redirectTo }));
    }else{
      yield put(mfaChallenge(mfaConfig))
    }
  } catch(e) {
    yield put(loginFailure(e));
  }
}

function* watchLogout() {
  yield takeLatest(types.AUTHENTICATION_LOGOUT, logout);
}

function* logout() {
  yield api.clearSession();
}

export default function*() {
  yield all([
    watchLogin(),
    watchLogout(),
    watchMFAConfirm(),
    load()
  ]);
}
