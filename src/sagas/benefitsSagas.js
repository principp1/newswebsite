import {
  all,
  put,
  takeLatest,
} from 'redux-saga/effects';
import { ActionTypes as types } from '../constants';
import {
  getBenefitFailure,
  loadBenefit,
} from '../actions/benefitsActions';
import { api } from '../services/api';

function* watchGetBenefit() {
  yield takeLatest(types.BENEFITS_GET_BENEFIT, getBenefit);
}

function* getBenefit(action) {
  const { type, slug } = action.data;
  try {
    const result = yield api.getBenefitContent(type, slug);
    yield put(loadBenefit(result));
  } catch (e) {
    yield put(getBenefitFailure(e));
  }
}

export default function*() {
  yield all([
    watchGetBenefit(),
  ]);
}
