import {
  all,
  put,
  takeLatest,
} from 'redux-saga/effects';
import { ActionTypes as types } from '../constants';
import {
  getPostsFail,
  getRecentPostsFail,
  getPostFail,
  loadPosts,
  loadRecentPosts,
  loadPost,
} from '../actions/blogActions';
import { api } from '../services/api';

function* watchGetPosts() {
  yield takeLatest(types.BLOG_GET_POSTS, getPostsHandler);
}

function* getPostsHandler({ data }) {
  const { page, startAt, pageSize } = data;
  try {
    const posts = yield api.getPosts({ page, startAt, pageSize });
    yield put(loadPosts(posts));
  } catch (e) {
    yield put(getPostsFail(e));
  }
}

function* watchGetRecentPosts() {
  yield takeLatest(types.BLOG_GET_RECENT_POSTS, getRecentPostsHandler);
}

function* getRecentPostsHandler() {
  try {
    const posts = yield api.getRecentPosts();
    yield put(loadRecentPosts(posts));
  } catch (e) {
    yield put(getRecentPostsFail(e));
  }
}

function* watchGetPost() {
  yield takeLatest(types.BLOG_GET_POST, getPostHandler);
}

function* getPostHandler({ data }) {
  try {
    const post = yield api.getPost(data);
    yield put(loadPost(post));
  } catch (e) {
    yield put(getPostFail(e));
  }
}

export default function*() {
  yield all([
    watchGetPosts(),
    watchGetRecentPosts(),
    watchGetPost(),
  ]);
}
