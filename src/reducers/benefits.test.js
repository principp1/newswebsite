import { ActionTypes as types } from '../constants';
import reducerTest from '../helpers/reducerTest';
import * as benefitsActions from '../actions/benefitsActions';

import benefits from './benefits';

const benefitPageData = {
  type: "wealth",
  slug: "foo",
  data: {  },
};

describe('benefits', () => {
  const initialState = {
    benefitPage: null,
    pageLoaded: false,
    getBenefitError: null,
  };

  reducerTest(benefits, initialState, {
    BENEFITS_GET_BENEFIT: {
      from: { pageLoaded: true },
      action: benefitsActions.getBenefit({ type: "wealth", slug: "foo" }),
      expect: { pageLoaded: false }
    },

    BENEFITS_GET_BENEFIT_SUCCESS: {
      from: {  },
      action: benefitsActions.loadBenefit(benefitPageData),
      expect: {
        benefitPage: benefitPageData,
        pageLoaded: true,
      },
    },
  });
});

