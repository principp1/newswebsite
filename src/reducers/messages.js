import {ActionTypes as types} from '../constants';

export const defaultState = {
    newMessageSource: null,
    list: [],
    // folder: "inbox",
    listLoaded: false,
    error: null,
    message: null,
    messageEdit: null,
    recipients: []

};

export default function messages(state = defaultState, action = {}) {
    const {type, data} = action;
    switch (type) {
        case types.MESSAGES_SET_FOLDER:
            return {
                ...state,
                error: null,
                folder: data
            };

        case types.MESSAGES_NEW_MESSAGE:
            return {
                ...state,
                error: null,
                newMessageSource: data.source,
            };

        case types.MESSAGES_GET_MESSAGE_SUCCESS:
            return {
                ...state,
                error: null,
                message: data
            };
        case types.MESSAGES_GET_MESSAGES:

            return {
                ...state,
                // list: data,
                error: null,
                listLoaded: false,
                folder: data.folder,
            };

        case types.MESSAGES_GET_MESSAGES_SUCCESS:

            return {
                ...state,
                error: null,
                list: data.results,
                listLoaded: true,
            };

        case types.MESSAGES_GET_MESSAGES_FAILURE:
            return {
                ...state,
                listLoaded: true,
                error: data.message,
            };
        case '@@message/RECIPIENTS':
            return {
                ...state,
                recipients: data,
            };
        default:
            return state;
    }
}


