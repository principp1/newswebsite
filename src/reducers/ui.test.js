import { ActionTypes as types } from '../constants';
import reducerTest from '../helpers/reducerTest';
import * as uiActions from '../actions/uiActions';
import * as authActions from '../actions/authenticationActions';

import ui from './ui';

describe('ui', () => {
  const initialState = {
    redirectTo: null,
  };

  reducerTest(ui, initialState, {
    UI_REDIRECT_CLEAR: {
      from: { redirectTo: '/somewhere-over-there' },
      action: uiActions.clearRedirect(),
      expect: {
        /* back to initial state */
      },
    },

    AUTHENTICATION_LOGIN_SUCCESS: {
      from: { redirectTo: null },
      action: authActions.loginSuccess({ redirectTo: '/there' }),
      expect: { redirectTo: '/there' },
    },
  });
});

