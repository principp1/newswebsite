import { ActionTypes as types } from '../constants';
import reducerTest from '../helpers/reducerTest';
import * as messagesActions from '../actions/messagesActions';

import messages from './messages';

const messagesList = [{
  id: "abc",
  subject: "Hello World",
  date: "2018-10-28T18:25:43.511Z",
  unread: false
}];

const messagesData = {
  page: 1,
  total: 38,
  messages: messagesList,
};

describe('messages', () => {
  const initialState = {
    newMessageSource: null,
    list: [],
    folder: "inbox",
    listLoaded: false,
    error: null,
  };

  reducerTest(messages, initialState, {
    MESSAGES_NEW_MESSAGE: {
      action: messagesActions.newMessage({ source: "/messages/sent" }),
      expect: {
        newMessageSource: "/messages/sent",
      },
    },

    MESSAGES_GET_MESSAGES: {
      from: { list: [], listLoaded: true },
      action: messagesActions.getMessages("inbox", { unread: true }),
      expect: {
        list: [],
        listLoaded: false,
      }
    },

    "MESSAGES_GET_MESSAGES change folder": {
      from: { list: [], listLoaded: true },
      action: messagesActions.getMessages("drafts", { unread: true }),
      expect: {
        list: [],
        listLoaded: false,
        folder: "drafts",
      }
    },

    MESSAGES_GET_MESSAGES_SUCCESS: {
      from: { list: [], listLoaded: false },
      action: messagesActions.loadMessages(messagesData),
      expect: { list: messagesList, listLoaded: true },
    },

    MESSAGES_GET_MESSAGES_FAILURE: {
      from: { listLoaded: false, error: null },
      action: messagesActions.getMessagesError(new Error("Something bad")),
      expect: { listLoaded: true, error: "Something bad" },
    },

  });
});


