import {ActionTypes as types} from '../constants';
import {SESSION_KEY} from "../utils";

const defaultState = {
    isLoggedIn: false,
    loginInProgress: false,
    showLogoutSuccessMessage: false,
    showLoginFailureMessage: null,
    secret: null,
    mfaToken: null,
    barcodeUri: null,
    otp: '',
    auth: JSON.parse(window.localStorage.getItem(SESSION_KEY)) || null,
};

export default function authentication(state = defaultState, action = {}) {
    const {type, data} = action;
    switch (type) {
        case types.AUTHENTICATION_SET_TOKEN:
            return {
                ...state,
                auth: data,
            };
        case types.AUTHENTICATION_LOGIN:
            return {
                ...state,
                loginInProgress: true,
            };

        case types.AUTHENTICATION_LOGIN_SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                loginInProgress: false,
                showLoginFailureMessage: null,
                secret: null,
                mfaToken: null,
                barcodeUri: null,
            };
        case types.AUTHENTICATION_MFA_CHALLENGE:
            return {
                ...state,
                secret: data.secret,
                mfaToken: data.mfa_token,
                barcodeUri: data.barcode_uri,
            };
        case types.AUTHENTICATION_LOGIN_FAILURE:
            return {
                ...state,
                loginInProgress: false,
                showLoginFailureMessage: true,
            };

        case types.AUTHENTICATION_LOGIN_FAILURE_CLEAR:
            return {
                ...state,
                showLoginFailureMessage: null,
            };

        case types.AUTHENTICATION_LOGGED_IN:
            return {
                ...state,
                isLoggedIn: true,
            };

        case types.AUTHENTICATION_LOGGED_OUT:
            return {
                ...state,
                isLoggedIn: false,
                showLoginFailureMessage: "You have been logged out",
            };

        case types.AUTHENTICATION_LOGOUT:
            return {
                ...state,
                isLoggedIn: false,
                showLogoutSuccessMessage: true,
            };

        case types.AUTHENTICATION_LOGOUT_CLEAR:
            return {
                ...state,
                showLogoutSuccessMessage: false,
            }

        default:
            return state;
    }
}

