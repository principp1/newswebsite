/**
 * Combines all the reducers and exposes them as one data structure.
 */

import {combineReducers} from 'redux';

import authentication from './authentication';
import benefits from '../pages/benefits/reducer';
import blog from './blog';
import footerReducer from '../components/footer/reducer';
import widgetReducer from '../components/widgets/reducer';
import layoutReducer from '../components/layout/reducer';
import messages from './messages';
import ui from './ui';
import { reducer } from 'react-redux-sweetalert';

import {reducer as formReducer} from 'redux-form'

export default combineReducers({
    authentication,
    benefits,
    form: formReducer,
    blog,
    messages,
    sweetalert: reducer,
    ui,
    footer: footerReducer,
    widget: widgetReducer,
    layout: layoutReducer,
});

