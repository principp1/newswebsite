import { ActionTypes as types } from '../constants';
import { blogState, validateReducer } from '../propTypes';

const defaultState = {
  posts: null,
  pageSize: 10,
  page: 1,
  postsCache: {},
  postsSequence: [],
  postsLoaded: false,

  post: null,
  postLoaded: false,

  recentPosts: null,
  recentPostsLoaded: false,

  error: null,
};

function cachePosts(cache, post) {
  return {
    ...cache,
    [post.slug]: post
  };
}

function blog(state = defaultState, action = {}) {
  const { type, data } = action;
  switch (type) {
    case types.BLOG_GET_POSTS:
      const { page, pageSize } = data;
      return {
        ...state,
        postsLoaded: false,
        page, pageSize
      };

    case types.BLOG_GET_POSTS_SUCCESS:
      const posts = data.results;
      return {
        ...state,
        error: null,
        posts: data,
        postsCache: posts.reduce(cachePosts, state.postsCache),
        postsSequence: state.postsSequence.concat(posts.map(post => post.slug)),
        postsLoaded: true,
      };

    case types.BLOG_GET_POSTS_FAILURE:
      return {
        ...state,
        postsLoaded: true,
        error: data,
      };

    case types.BLOG_GET_RECENT_POSTS:
      return {
        ...state,
        recentPostsLoaded: false,
      };

    case types.BLOG_GET_RECENT_POSTS_SUCCESS:
      return {
        ...state,
        recentPosts: data,
        recentPostsLoaded: true,
      };

    case types.BLOG_GET_RECENT_POSTS_FAILURE:
      return {
        ...state,
        recentPostsLoaded: true,
      };

    case types.BLOG_GET_POST:
      return {
        ...state,
        postLoaded: false,
      };

    case types.BLOG_GET_POST_SUCCESS:
      return {
        ...state,
        postLoaded: true,
        post: data,
        error: null,
      };

    case types.BLOG_GET_POST_FAILURE:
      return {
        ...state,
        postLoaded: true,
        error: data,
      };

    default:
      return state;
  }
};

export default validateReducer(blogState, 'blog')(blog);
