import { ActionTypes as types } from '../constants';
import reducerTest from '../helpers/reducerTest';
import * as authActions from '../actions/authenticationActions';

import authentication from './authentication';

describe('authentication', () => {
  const initialState = {
    isLoggedIn: false,
    loginInProgress: false,
    showLogoutSuccessMessage: false,
    showLoginFailureMessage: null,
  };

  reducerTest(authentication, initialState, {
    AUTHENTICATION_LOGIN: {
      from: { loginInProgress: false },
      action: authActions.login({ username: 'foo', password: 'bar' }),
      expect: { loginInProgress: true }
    },

    AUTHENTICATION_LOGGED_IN: {
      from: { isLoggedIn: false },
      action: authActions.loggedIn(),
      expect: { isLoggedIn: true },
    },

    AUTHENTICATION_LOGGED_OUT: {
      from: { isLoggedIn: true },
      action: authActions.loggedOut(),
      expect: {
        isLoggedIn: false,
        showLoginFailureMessage: "You have been logged out"
      }
    },

    AUTHENTICATION_LOGIN_SUCCESS: {
      from: { isLoggedIn: false, loginInProgress: true },
      action: authActions.loginSuccess({}),
      expect: { isLoggedIn: true, loginInProgress: false },
    },

    "AUTHENTICATION_LOGIN_SUCCESS with failure message": {
      from: {
        isLoggedIn: false,
        showLoginFailureMessage: true,
        loginInProgress: true,
      },
      action: authActions.loginSuccess({}),
      expect: {
        isLoggedIn: true,
        showLoginFailureMessage: null,
        loginInProgress: false,
      },
    },

    AUTHENTICATION_LOGIN_FAILURE: {
      from: { loginInProgress: true },
      action: authActions.loginFailure(),
      expect: { showLoginFailureMessage: true, loginInProgress: false }
    },

    AUTHENTICATION_LOGIN_FAILURE_CLEAR: {
      from: { showLoginFailureMessage: true },
      action: authActions.clearLoginFailureMessage(),
      expect: { showLoginFailureMessage: null }
    },

    AUTHENTICATION_LOGOUT: {
      from: { isLoggedIn: true },
      action: authActions.logout(),
      expect: {
        isLoggedIn: false,
        showLogoutSuccessMessage: true
      }
    },

    AUTHENTICATION_LOGOUT_CLEAR: {
      from: { showLogoutSuccessMessage: true },
      action: authActions.clearLogoutSuccessMessage(),
      expect: { showLogoutSuccessMessage: false },
    },
  });
});


