import { ActionTypes as types } from '../constants';

export const defaultState = {
  redirectTo: null,
};

export default function ui(state = defaultState, action = {}) {
  const { type, data } = action;
  switch (type) {
    case types.UI_REDIRECT_CLEAR:
      return {
        ...state,
        redirectTo: null,
      };

    case types.AUTHENTICATION_LOGIN_SUCCESS:
      return {
        ...state,
        redirectTo: data.redirectTo,
      };

    default:
      return state;
  }
}

