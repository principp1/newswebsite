// import { ActionTypes as types } from '../constants';
// import { benefitsState, validateReducer } from '../propTypes';
//
// const defaultState = {
//   benefitPage: null,
//   pageLoaded: false,
//   getBenefitError: null,
// };
//
// function benefits(state = defaultState, action = {}) {
//   switch (action.type) {
//     case types.BENEFITS_GET_BENEFIT:
//       return {
//         ...state,
//         pageLoaded: false,
//         benefitPage: null,
//         getBenefitError: null,
//       };
//
//     case types.BENEFITS_GET_BENEFIT_SUCCESS:
//       return {
//         ...state,
//         benefitPage: action.data,
//         pageLoaded: true,
//         getBenefitError: null,
//       };
//
//     case types.BENEFITS_GET_BENEFIT_FAILURE:
//       return {
//         ...state,
//         pageLoaded: true,
//         getBenefitError: action.data
//       };
//
//     default:
//       return state;
//   }
// }
//
// export default validateReducer(benefitsState, 'benefits')(benefits);
