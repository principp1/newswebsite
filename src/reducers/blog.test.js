import { ActionTypes as types } from '../constants';
import reducerTest from '../helpers/reducerTest';
import * as blogActions from '../actions/blogActions';

import blog from './blog';
import sampleBlogPosts from '../fixtures/sampleBlogPosts.json';

const sampleArticle = sampleBlogPosts.results[0];

describe('blog', () => {
  const error = new Error("Something went wrong");

  const initialState = {
    posts: null,
    pageSize: 10,
    page: 1,
    postsLoaded: false,

    post: null,
    postLoaded: false,

    recentPosts: null,
    recentPostsLoaded: false,

    error: null,
  };

  reducerTest(blog, initialState, {
    BLOG_GET_POSTS: {
      action: blogActions.getPosts({ page: 2, pageSize: 10 }),
      expect: { pageSize: 10, page: 2, postsLoaded: false }
    },

    BLOG_GET_POSTS_SUCCESS: {
      from: {
        error,
        postsLoaded: false,
      },
      action: blogActions.loadPosts(sampleBlogPosts),
      expect: {
        error: null,
        posts: sampleBlogPosts,
        postsLoaded: true
      }
    },

    BLOG_GET_POSTS_FAILURE: {
      action: blogActions.getPostsFail(error),
      expect: {
        postsLoaded: true,
        error: error
      }
    },

    BLOG_GET_POST: {
      from: { postLoaded: true },
      action: blogActions.getPost('the-post-slug'),
      expect: { postLoaded: false }
    },

    BLOG_GET_POST_SUCCESS: {
      from: { error, postLoaded: false, post: null },
      action: blogActions.loadPost(sampleArticle),
      expect: { error: null, postLoaded: true, post: sampleArticle }
    },

    BLOG_GET_POST_FAILURE: {
      from: { postLoaded: false },
      action: blogActions.getPostFail(error),
      expect: { postLoaded: true, error }
    },
  });
});
