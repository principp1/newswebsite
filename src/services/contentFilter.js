const fieldRegex = /\\?\[([^\]]+)]/g;


export function contentFilter(rawContent, fields) {
    let content;
    switch (typeof rawContent) {
        case 'string':
            content = rawContent;
            break;
        case 'undefined':
            content = "";
            break;
        case 'number':
            content = rawContent.toString();
            break;
        default:
            content = rawContent ? rawContent.toString() : '';
    }
    if (fieldRegex.test(content) < 0) {
        return content;
    }

    const result = content.replace(fieldRegex, (matched, field) => {
        if (
            fields.hasOwnProperty(field)
            && fields[field] !== null
            && fields[field] !== undefined
        ) {
            return fields[field];
        } else {
            // return clean(matched);
            return '';
        }
    });
    return result === '' ? null : result;
}

export default contentFilter;

