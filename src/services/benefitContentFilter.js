import contentFilter from './contentFilter';

function flattenEmployeeData(data, productId) {
    const productData = data[productId] || {};
    const generalData = productData.General || {};
    return {
        ...data,
        ...productData,
        ...generalData,
    };
}

// TODO: Move this to configuration
function remapFields(data) {
    return {
        ...data,
        "Company Name": data.employer,
        "incomeMultiple": data.incomeMultiple || data.multiple,
    };
}

function groupFilter(groups, data) {
    return groups.map(group => ({
        title: contentFilter(group.title, data),
        content: contentFilter(group.content, data),
    }));
}

function providerFilter(provider, data) {
    return {
        ...provider,
        content: contentFilter(provider.content, data),
        logo: contentFilter(provider.logo, data),
        name: contentFilter(provider.name, data),
        video: contentFilter(provider.video, data),
    };
}

const filterMappings = {
    intro: contentFilter,
    contentSection: groupFilter,
    faq: groupFilter,
    provider: providerFilter,
    statusMessage: (_, data) => data.status
};

function benefitContentFilter(content, data, productId) {
    if (!productId) {
        return content;
    }
    const fieldData = remapFields(flattenEmployeeData(data, productId));
    const newContent = {};
    for (let key in filterMappings) {
        const filter = filterMappings[key];
        const newValue = filter(content[key], fieldData);
        newContent[key] = newValue;
    }
    return {...content, ...newContent};
}

export default benefitContentFilter;
