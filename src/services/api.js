import axios from 'axios';
import oauth from 'axios-oauth-client';
import tokenProvider from 'axios-token-interceptor';

import Session from './session';
import queryString from 'query-string';
import moment from 'moment-timezone';

import {getAuthHeaders, requestInterceptor, responseInterceptor} from "../utils";

function verifyJsonObj(response) {
    if (typeof response.data !== "object") {
        throw Error('Response was not a valid JSON object');
    }
    return response;
}

function responseData(response) {
    return response.data;
}

class Api {
    constructor({url, clientId, clientSecret}) {
        this.url = url;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.session = new Session();
        this.setupClient();
        this.authClient = axios.create({
            auth: {
                username: this.clientId,
                password: this.clientSecret,
            },
        });
        this.onUnauthorizedListeners = [];
    }

    onUnauthorized(fn) {
        this.onUnauthorizedListeners.push(fn);
    }

    setupClient() {
        this.client = axios.create();
        const accessToken = this.session.get('access_token');

        this.client.interceptors.request.use(tokenProvider({
            getToken: () => accessToken
        }));
        this.client.interceptors.request.use(requestInterceptor, responseInterceptor);
        this.client.interceptors.request.use(tokenProvider({
            getToken: () => accessToken
        }));

    }

    async authenticate({username, password, rememberMe}) {

        this.session.set('refreshing', "yes");
        const result = await oauth.client(this.authClient, {
            url: this.urlForPath('/authorize/'),
            grant_type: 'password',
            username,
            password,
        })();

        result.rememberMe = rememberMe;
        if (!rememberMe) {
            delete result.refresh_token;
        }

        result.expires = moment().add(result.expires_in, 'seconds').valueOf();

        const mfaCheck = await axios.post(
            this.urlForPath('/mfa/check/'),
            {email: username, password},
            {
                headers: {'Authorization': `${result.token_type} ${result.access_token}`}
            }
        );
        const mfaConfig = mfaCheck.data;

        if ('mfa_token' in mfaConfig) {
            // SECURITY FAIL, endpoint must only return access_token if mfa
            // is enabled and complete
            window.localStorage.setItem('_auth_temp', JSON.stringify(result));
            // this.session.set("__cached_session", result);
            return mfaConfig;
        } else {
            this.session.set(result);
            this.setupClient();
            return false;
        }
    }

    async mfaConfirm({mfa_token, otp}) {
        const mfaCheck = await axios.post(
            this.urlForPath('/mfa/confirm/'),
            {mfa_token, otp},
        );
        const mfaConfig = mfaCheck.data;

        if ('access_token' in mfaConfig) {
            const auth = JSON.parse(window.localStorage.getItem('_auth_temp'));
            window.localStorage.clear();
            this.session.set(auth);
            this.setupClient();
        }
    }

    // TODO: Deal with multiple concurrent refreshes
    async refresh() {

        const result = await oauth.client(this.authClient, {
            url: this.urlForPath('/authorize/'),
            grant_type: 'refresh_token',
            refresh_token: this.session.get('refresh_token'),
        })();
        this.session.set(result);
        this.setupClient();
    }

    async clearSession() {
        await axios.post(this.urlForPath('/revoke_token/'), queryString.stringify({
            token: this.session.get('access_token')
        }), {
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            auth: {
                username: this.clientId,
                password: this.clientSecret,
            },
        });
        return this.session.clear();
    }

    unauthorized(e) {
        this.session.clear();
        this.onUnauthorizedListeners.forEach(fn => fn(e));
    }

    hasToken() {
        return !!this.session.get('access_token');
    }

    async isSessionValid() {
        // TODO: temporary...
        const response = await this.get(this.urlForPath('/employee/'))
        return !!response;
    }

    isLoggedIn() {
        return this.hasToken();
    }

    urlForPath(path) {
        // see /src/index.js#L10
        return `${this.url}${path}`;
        // return path;
    }

    isUnauthorized(e) {
        return e.response && e.response.status === 401;
    }

    async get(url) {
        try {
            return await this.client.get(url);
        } catch (e) {
            if (this.isUnauthorized(e)) {
                if (this.session.get('rememberMe')) {
                    try {
                        await this.refresh();
                    } catch (e) {
                        this.unauthorized(e);
                        throw e;
                    }
                    return await this.client.get(url);
                } else {
                    this.unauthorized(e);
                }
            } else {
                throw e;
            }
        }
    }

    // TODO: This is hardcoded data. Pull from actual cms stream
    getBenefitCmsContent(type, slug) {
        // return this.client.get(`/dummy-data/${type}/${slug}.json`)
        const productId = this.productIdFromSlug(slug);
        return this.client.get(this.urlForPath(`/cms/${productId}/`))
            .then(verifyJsonObj)
            .then(response => {

                return {
                    type, slug,
                    data: response.data
                }
            });
    }

    async getBenefitContent(type, slug) {
        const contentContainer = await this.getBenefitCmsContent(type, slug);
        return {
            ...contentContainer,
            // data: newContent
        }
    }

    async getMessages(folder, {unread}) {
        const data = {
            params: {
                folder,
                unread: unread ? 1 : 0
            }
        };
        try {

            return await this.client.get(this.urlForPath('/messages/'), data)
        } catch (e) {
            if (this.isUnauthorized(e)) {
                if (this.session.get('rememberMe')) {
                    try {
                        await this.refresh();
                    } catch (e) {
                        this.unauthorized(e);
                        throw e;
                    }
                    return await this.client.get(this.urlForPath('/messages/'), data)
                } else {
                    this.unauthorized(e);
                }
            }
        }


    }

    getMessage(id) {
        return axios.get(
            this.urlForPath(`/messages/${id}/`),
            {headers: getAuthHeaders(),}
        )
            .then(responseData)
            .then(data => data);
    }

    newMessage(data) {
        const {id} = data;
        return (id ? axios.patch : axios.post)(
            this.urlForPath(id ? `/messages/${id}/` : `/messages/`),
            data,
            {headers: getAuthHeaders(),}
        )
            .then(responseData)
            .then(data => data);
    }

    async getPosts({startAt, pageSize}) {
        const before_of = startAt
        const page_size = pageSize;
        const searchParams = queryString.stringify({before_of, page_size});
        return this.get(this.urlForPath(`/blog/post/?${searchParams}`))
            .then(responseData);
    }

    async getRecentPosts() {
        return this.getPosts({page: 1, pageSize: 4});
    }

    async getPost(slug) {
        return this.get(this.urlForPath(`/blog/post/${slug}/`))
            .then(responseData);
    }
}

export const api = new Api({
    url: process.env.REACT_APP_API_URL,
    clientId: process.env.REACT_APP_CLIENT_ID,
    clientSecret: process.env.REACT_APP_CLIENT_SECRET,
});

export default Api;
