import contentFilter from './contentFilter';

describe('contentFilter', () => {
  const fields = {
    pet: "horse",
    color: "green",
    "friendly animal": "dog",
    nullField: null,
  };
  const testData = [
    {
      text: "The quick brown fox",
      expected: "The quick brown fox",
    },
    {
      text: "The quick [kulay] fox",
      expected: "The quick [kulay] fox",
    },
    {
      text: "The quick [color] fox",
      expected: "The quick green fox",
    },
    {
      text: "The quick [color] [pet]",
      expected: "The quick green horse",
    },
    {
      text: "[color] quick [color] [pet]",
      expected: "green quick green horse",
    },
    {
      text: "The quick [color] [pet] jumped over the lazy [friendly animal]",
      expected: "The quick green horse jumped over the lazy dog",
    },
    {
      text: "The quick \\[color\\] [pet]",
      expected: "The quick [color] horse",
    },
    {
      text: "[color]",
      expected: "green",
    },
    {
      text: null,
      expected: "",
    },
    {
      text: 0,
      expected: "0",
    },
    {
      text: "I have a [nullField] here.",
      expected: "I have a [nullField] here.",
    }
  ];

  testData.forEach(({ text, expected }) => {
    it(`matches "${text}" correctly`, () => {
      expect(contentFilter(text, fields)).toEqual(expected);
    });
  })
});
