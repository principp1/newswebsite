import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {clearLogoutSuccessMessage, login, mfaConfirm} from '../actions/authenticationActions';
import LoginPage from '../components/login/LoginPage';

import {Helmet} from "react-helmet";

class Login extends Component {
    static propTypes = {
        showLogoutSuccessMessage: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleLogin(data) {
        this.props.dispatch(login(data));
    }

    componentWillUnmount() {
        const {dispatch, showLogoutSuccessMessage} = this.props;
        if (showLogoutSuccessMessage) {
            dispatch(clearLogoutSuccessMessage());
        }
    }

    render() {
        return <div>
            <Helmet>
                <title>Login page</title>
            </Helmet>
            <LoginPage
                onLogin={this.handleLogin}
                onMFAConfirm={data => this.props.dispatch(mfaConfirm(data))}
                {...this.props}
            />
        </div>
    }
}

export default connect(({authentication}) => authentication)(Login);
