import React from 'react';

import MainLayout from '../components/layout/MainLayout';
import SiteContainer from '../components/SiteContainer';
import { connect } from 'react-redux';

function notFoundContent(path) {
  return (
    <div style={{ minHeight: '100vh', padding: '100px 40px' }} className="NotFound">
      <h1>Page Not Found</h1>
      <pre>
        path: { path }
      </pre>
    </div>
  );
}

function NotFound({ location, isLoggedIn }) {
  const content = notFoundContent(location.pathname);
  return isLoggedIn
    ? <MainLayout>{ content }</MainLayout>
    : <SiteContainer>{ content }</SiteContainer>
  ;
}

export default connect(({ authentication }) => {
  const { isLoggedIn } = authentication;
  return { isLoggedIn };
})(NotFound);
