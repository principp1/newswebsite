import React, { Component } from 'react';

import DocumentsPage from '../components/documents/DocumentsPage';

class Documents extends Component {
  render() {
    return (
      <DocumentsPage />
    );
  }
}

export default Documents;
