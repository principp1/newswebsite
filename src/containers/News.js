import React from 'react';
import { Route, Switch } from 'react-router-dom';

import MainPage from '../components/news/MainPage';
import ArticlePage from '../components/news/ArticlePage';

function News() {
  return (
    <Switch>
      <Route path="/news" exact component={ MainPage } />
      <Route path="/news/:slug" component={ ArticlePage } />
    </Switch>
  );
}

export default News;
