import React, {Component} from 'react';

import MessagesPage from '../components/messages/MessagesPage';
import '../components/messages/styles.scss';
class Messages extends Component {

    render() {
        return (
            <MessagesPage/>
        );
    }
}

export default Messages;
