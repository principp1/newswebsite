import React, {Component} from 'react';

import {Route, Switch} from 'react-router-dom';

import Benefit from '../pages/benefits/Benefit';
import Wealth from '../components/homepages/Wealth';
import Health from '../components/homepages/Health';
import Lifestyle from '../components/homepages/Lifestyle';

class Benefits extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/wealth" component={Wealth}/>
                <Route exact path="/health" component={Health}/>
                <Route exact path="/lifestyle" component={Lifestyle}/>

                <Route path="/:type/:slug" component={({match, location}) => {
                    const {type, slug} = match.params;
                    return (
                        <Benefit
                            type={type}
                            slug={slug}
                            location={location}
                        />
                    );
                }}/>
            </Switch>
        );
    }
}

export default Benefits;
