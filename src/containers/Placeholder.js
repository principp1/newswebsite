import React from 'react';

import MainLayout from '../components/layout/MainLayout';
import SiteContainer from '../components/SiteContainer';

function Placeholder({ title }) {
  return (
    <MainLayout>
      <SiteContainer>
        <h1>{ (title || "This is a Placeholder") }</h1>
        <p>This is a placeholder</p>
      </SiteContainer>
    </MainLayout>
  );
}

export default Placeholder;
