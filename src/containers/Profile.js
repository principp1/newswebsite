import React, { Component } from 'react';

import ProfilePage from '../components/profile/ProfilePage';

class Profile extends Component {

  render() {
    return (
      <ProfilePage />
    );
  }
}

export default Profile;
