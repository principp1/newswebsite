var nice = false;
jQuery(document).ready(function () {
    var SiteContainer = $('.site-container');
    Pace.on('done', function() {
        SiteContainer.removeClass('loadingHide');
        if (Modernizr.mq('only screen and (min-width: 992px)')) {
            var nice = $('html').niceScroll({
                styler:"fb",
                cursorcolor: "rgba(0, 0, 0, 1)", // change cursor color in hex
                zindex: 99999,
                mousescrollstep: 100
            }).resize();
        }
    });

});
/*
 * Replace all SVG images with inline SVG
 */
$(function() {
    $('img.svg-img').each(function() {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');
// Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }
// Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
// Replace image with new SVG
            $img.replaceWith($svg);
        }, 'xml');
    });
})